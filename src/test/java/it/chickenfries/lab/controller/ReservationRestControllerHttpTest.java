package it.chickenfries.lab.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.chickenfries.lab.dto.reservation.ReservationRequestDto;
import it.chickenfries.lab.model.DirectionType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:reservation.sql")
public class ReservationRestControllerHttpTest {

    private MockMvc mockMvc;
    @Autowired private WebApplicationContext wac;
    private final String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void getReservationsHttpTest200Ok() throws Exception {

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/reservations/1/".concat(today));

        mockMvc.perform(request).
                andDo(MockMvcResultHandlers.print()).
                andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void newReservationHttpTest201Created() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        ReservationRequestDto reservationRequestDto = ReservationRequestDto.builder().
                //children(Collections.singletonList(new Child("Alan",false,null))).
                direction(DirectionType.TO_SCHOOL).
                place("Parrocchia Gesù").build();
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.
                post("/reservations/1/".concat(today)).
                contentType(MediaType.APPLICATION_JSON).
                content(objectMapper.writeValueAsString(reservationRequestDto));
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth("");
        request.headers(httpHeaders);

        mockMvc.perform(request).
                andDo(MockMvcResultHandlers.print()).
                andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void getReservationHttpTest200Ok() throws Exception {

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/reservations/1/".concat(today).concat("/1"));

        mockMvc.perform(request).
                andDo(MockMvcResultHandlers.print()).
                andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void updateReservationHttpTest200Ok() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        ReservationRequestDto reservationRequestDto = ReservationRequestDto.builder().
                //children(Arrays.asList(new Child("Alan",false,null),new Child("Rick",false,null))).
                direction(DirectionType.TO_SCHOOL).
                place("Giardino Luigi Martini").build();
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.
                put("/reservations/1/".concat(today).concat("/3")).
                contentType(MediaType.APPLICATION_JSON).
                content(objectMapper.writeValueAsString(reservationRequestDto));

        mockMvc.perform(request).
                andDo(MockMvcResultHandlers.print()).
                andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void deleteReservationHttpTest200Ok() throws Exception {

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.
                delete("/reservations/1/".concat(today).concat("/1"));

        mockMvc.perform(request).
                andDo(MockMvcResultHandlers.print()).
                andExpect(MockMvcResultMatchers.status().isOk());

    }

}
