package it.chickenfries.lab.dto.children;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChildReservationStatusResponseDto {

    private String name;
    private String surname;
    private String email;

    private LocalDate date;

    private boolean reservedToHome;
    private String lineToHome;
    private String ticketToHome;

    private boolean reservedToSchool;
    private String lineToSchool;
    private String ticketToSchool;

}
