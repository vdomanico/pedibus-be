package it.chickenfries.lab.dto.children;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChildReservationResponseDto {

    private String name;
    private String surname;
    private String email;
    private String ticket;
    private boolean collected;
    private boolean delivered;

}
