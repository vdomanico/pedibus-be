package it.chickenfries.lab.dto.children;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReservationChildRequestDto {

    @NotBlank
    private String name;
    @NotBlank
    private String surname;
    @NotBlank
    private String email;

}
