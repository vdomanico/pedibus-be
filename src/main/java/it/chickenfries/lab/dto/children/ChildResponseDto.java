package it.chickenfries.lab.dto.children;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChildResponseDto {

    private String name;
    private String surname;
    private String email;

}
