package it.chickenfries.lab.dto.children;

import lombok.Data;

@Data
public class ChildDeliverRequestDto {

    private String ticket;
    private boolean deliver;

}
