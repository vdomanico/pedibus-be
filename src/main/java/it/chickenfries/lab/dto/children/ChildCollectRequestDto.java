package it.chickenfries.lab.dto.children;

import lombok.Data;

@Data
public class ChildCollectRequestDto {

    private String ticket;
    private boolean collect;

}
