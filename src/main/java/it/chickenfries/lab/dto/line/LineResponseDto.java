package it.chickenfries.lab.dto.line;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LineResponseDto {

    private String line;
    private List<LineStopResponseDto> toSchool;
    private List<LineStopResponseDto> toHome;

}
