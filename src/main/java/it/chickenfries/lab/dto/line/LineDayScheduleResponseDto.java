package it.chickenfries.lab.dto.line;

import it.chickenfries.lab.dto.schedule.UserLineScheduleStatusResponseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LineDayScheduleResponseDto {

    private String line;
    private LocalDate day;
    private List<UserLineScheduleStatusResponseDto> users;

}
