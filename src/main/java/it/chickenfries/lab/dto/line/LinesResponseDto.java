package it.chickenfries.lab.dto.line;

import lombok.Data;

import java.util.List;

@Data
public class LinesResponseDto {

    private List<String> lines;

}
