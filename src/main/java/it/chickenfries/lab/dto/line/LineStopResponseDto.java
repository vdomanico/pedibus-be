package it.chickenfries.lab.dto.line;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import it.chickenfries.lab.model.SqlTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LineStopResponseDto {

    private String name;
    @JsonSerialize(using = SqlTimeSerializer.class) private Time time;

}
