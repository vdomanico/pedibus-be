package it.chickenfries.lab.dto.schedule;

import it.chickenfries.lab.model.DirectionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShiftResponseDto {

    private String line;
    private LocalDate date;
    private DirectionType direction;
    private String name;
    private String surname;
    private String email;

}
