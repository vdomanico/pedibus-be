package it.chickenfries.lab.dto.schedule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LineShiftResponseDto {

    private String line;
    private List<ShiftScheduleDto> shifts;

}
