package it.chickenfries.lab.dto.schedule;

import it.chickenfries.lab.dto.user.UserResponseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserLineScheduleStatusResponseDto {

    private UserResponseDto userDetail;
    private UseStatusResponseDto to_school;
    private UseStatusResponseDto to_home;

}
