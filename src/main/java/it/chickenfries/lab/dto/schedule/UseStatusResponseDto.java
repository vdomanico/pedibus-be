package it.chickenfries.lab.dto.schedule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UseStatusResponseDto {

    private boolean shift;
    private boolean available;
    private boolean reservedElseWhere;
    private String lineElseWhere;

}
