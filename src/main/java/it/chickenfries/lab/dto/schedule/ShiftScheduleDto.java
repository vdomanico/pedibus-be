package it.chickenfries.lab.dto.schedule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShiftScheduleDto {

    private String line;
    private LocalDate date;
    private boolean toHome;
    private boolean toSchool;

}
