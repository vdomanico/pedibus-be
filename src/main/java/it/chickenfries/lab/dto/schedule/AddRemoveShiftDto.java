package it.chickenfries.lab.dto.schedule;

import it.chickenfries.lab.model.DirectionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddRemoveShiftDto {

    @NotBlank private String line;
    @NotBlank String day;
    @NotBlank private String email;
    private DirectionType direction;
    @NotNull private Boolean add;

}
