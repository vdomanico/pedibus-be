package it.chickenfries.lab.dto.user;

import lombok.Data;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RecoverRequestDto {

    @NotNull
    @Size(min = 5)
    private String password;
    @NotNull @Size(min = 5)
    private String confirmPassword;

    @AssertTrue(message = "Password and confirmation password mismatch")
    public boolean isPasswordConfirmed(){
        return password!=null && password.equals(confirmPassword);
    }

}
