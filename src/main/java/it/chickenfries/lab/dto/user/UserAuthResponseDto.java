package it.chickenfries.lab.dto.user;

import it.chickenfries.lab.dto.children.ChildResponseDto;
import it.chickenfries.lab.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserAuthResponseDto {

    private String token;
    private String name;
    private String surname;
    private String email;
    private Map<UserRole, List<String>> roles;
    private List<ChildResponseDto> children;
    private int unreadMessages;

}
