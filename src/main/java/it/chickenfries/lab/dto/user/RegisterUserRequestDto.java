package it.chickenfries.lab.dto.user;

import it.chickenfries.lab.model.UserRole;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.Set;

@Data
public class RegisterUserRequestDto {

    @NotNull
    @Email
    private String email;
    @NotNull @Size(min = 5)
    private String pwd;
    @NotBlank
    private String name;
    @NotBlank
    private String surname;
    @NotNull @Size(min = 5)
    private String confirmationPwd;
    @NotNull
    private Set<UserRole> roles;
    @NotNull
    private String line;

    @AssertTrue(message = "Password and confirmation password mismatch")
    public boolean isPasswordConfirmed(){
        return pwd!=null && pwd.equals(confirmationPwd);
    }

}
