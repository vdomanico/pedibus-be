package it.chickenfries.lab.dto.user;

import it.chickenfries.lab.model.UserRole;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class UserResponseDto {

    private String email;
    private String name;
    private String surname;
    private boolean enable;
    private Map<UserRole, List<String>> roles;

}
