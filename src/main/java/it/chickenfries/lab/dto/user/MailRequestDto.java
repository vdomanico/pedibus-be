package it.chickenfries.lab.dto.user;

import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class MailRequestDto {

    @Email
    private String email;

}
