package it.chickenfries.lab.dto.user;

import lombok.Data;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UpdatePasswordRequestDto {

    @NotNull
    private String currentPassword;
    @NotNull

    @Size(min = 5)
    private String newPassword;

    @NotNull @Size(min = 5)
    private String confirmPassword;

    @AssertTrue(message = "Password and confirmation password mismatch")
    public boolean isPasswordConfirmed(){
        return newPassword!=null && newPassword.equals(confirmPassword);
    }

}
