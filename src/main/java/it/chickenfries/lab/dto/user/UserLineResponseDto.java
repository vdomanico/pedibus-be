package it.chickenfries.lab.dto.user;

import it.chickenfries.lab.model.UserRole;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserLineResponseDto {

    private String email;
    private String name;
    private String surname;
    private boolean enable;
    private List<UserRole> roles;

}
