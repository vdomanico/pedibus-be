package it.chickenfries.lab.dto.user;

import it.chickenfries.lab.model.UserRole;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class LineUserGrantRequestDto {

    @NotBlank private String line;
    @NotNull private boolean enable;
    @NotBlank private String email;
    @NotNull private UserRole role;

}
