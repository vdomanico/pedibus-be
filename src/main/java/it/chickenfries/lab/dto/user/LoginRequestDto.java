package it.chickenfries.lab.dto.user;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class LoginRequestDto {

    @NotNull
    @Email
    private String email;
    @NotNull private String password;

}
