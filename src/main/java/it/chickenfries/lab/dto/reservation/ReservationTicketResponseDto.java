package it.chickenfries.lab.dto.reservation;

import it.chickenfries.lab.dto.children.ChildResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class ReservationTicketResponseDto {

    private Map<String, ChildResponseDto> tickets;

}
