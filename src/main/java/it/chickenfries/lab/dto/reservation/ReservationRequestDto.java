package it.chickenfries.lab.dto.reservation;

import it.chickenfries.lab.dto.children.ReservationChildRequestDto;
import it.chickenfries.lab.model.DirectionType;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Builder
public class ReservationRequestDto {

    @NotEmpty private List<ReservationChildRequestDto> children;
    @NotBlank private String place;
    private DirectionType direction;
}
