package it.chickenfries.lab.dto.reservation;

import it.chickenfries.lab.dto.children.ReservationChildRequestDto;
import it.chickenfries.lab.model.DirectionType;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class ReservationUpdateRequestDto {

    @NotBlank private String ticket;
    @NotNull private ReservationChildRequestDto child;
    @NotBlank private String place;
    private DirectionType direction;

}
