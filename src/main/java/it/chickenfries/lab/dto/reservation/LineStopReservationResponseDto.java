package it.chickenfries.lab.dto.reservation;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import it.chickenfries.lab.dto.children.ChildReservationResponseDto;
import it.chickenfries.lab.model.SqlTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LineStopReservationResponseDto {

    private String place;
    @JsonSerialize(using = SqlTimeSerializer.class) private Time time;
    private List<ChildReservationResponseDto> onBoard;
    private List<ChildReservationResponseDto> offBoard;
    private double latitude;
    private double longitude;
}
