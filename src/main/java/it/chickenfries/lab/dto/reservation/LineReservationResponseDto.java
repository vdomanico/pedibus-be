package it.chickenfries.lab.dto.reservation;

import it.chickenfries.lab.dto.children.ChildReservationStatusResponseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LineReservationResponseDto {

    private String line;
    private LocalDate day;
    private List<LineStopReservationResponseDto> toSchool;
    private List<LineStopReservationResponseDto> toHome;
    private List<ChildReservationStatusResponseDto> children;

}
