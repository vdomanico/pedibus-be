package it.chickenfries.lab.dto.reservation;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import it.chickenfries.lab.dto.children.ChildResponseDto;
import it.chickenfries.lab.model.SqlTimeSerializer;
import lombok.Builder;
import lombok.Data;

import java.sql.Time;
import java.time.LocalDate;

@Data
@Builder
public class ReservationDetailResponseDto {

    private String ticketId;
    private ChildResponseDto child;
    private String onBoard;
    @JsonSerialize(using = SqlTimeSerializer.class) private Time timeOnBoard;
    private String offBoard;
    @JsonSerialize(using = SqlTimeSerializer.class) private Time timeOffBoard;
    private LocalDate day;

}
