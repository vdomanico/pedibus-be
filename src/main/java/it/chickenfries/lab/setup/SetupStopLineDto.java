package it.chickenfries.lab.setup;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import it.chickenfries.lab.model.SqlTimeDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SetupStopLineDto {

    private String place;
    @JsonDeserialize(using = SqlTimeDeserializer.class)
    private Time toSchool;
    @JsonDeserialize(using = SqlTimeDeserializer.class)
    private Time toHome;
    private double lat;
    private double lon;

}
