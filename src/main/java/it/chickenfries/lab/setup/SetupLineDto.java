package it.chickenfries.lab.setup;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SetupLineDto {

    private String line;
    private SetupUserDto admin;
    private List<SetupStopLineDto> places;
}
