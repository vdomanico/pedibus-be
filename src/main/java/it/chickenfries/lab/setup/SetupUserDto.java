package it.chickenfries.lab.setup;

import it.chickenfries.lab.dto.children.RegisterChildRequestDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SetupUserDto {
    private String email;
    private String name;
    private String surname;
    private List<RegisterChildRequestDto> children;
}
