package it.chickenfries.lab.setup;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.persistency.entity.*;
import it.chickenfries.lab.repository.LineRepository;
import it.chickenfries.lab.repository.UserRepository;
import it.chickenfries.lab.service.MailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class StartupService {

    private static final Logger LOGGER = LogManager.getLogger(StartupService.class.getCanonicalName());
    private final LineRepository lineRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final MailService mailService;
    private static final String[] lines = {"line1.json","line2.json","line3.json"};
    @Value("${fe-url}") private String confirmationUrl;
    @Value("${setup-password}") private String password;

    @Autowired
    public StartupService(
            LineRepository lineRepository,
            UserRepository userRepository,
            PasswordEncoder passwordEncoder,
            MailService mailService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.lineRepository = lineRepository;
        this.mailService = mailService;
    }

    @PostConstruct
    public void init(){
        LOGGER.info("************************* SETUP ************************** SETUP ************************ SETUP ***************************");
        LOGGER.info("********************************************************** LINES **********************************************************");
        for (String line : lines) {
            InputStream inputStream = getClass().getResourceAsStream("/lines/" + line);
            loadLine(inputStream, line);
        }
    }

    @Transactional
    public void loadLine(InputStream inputStream, String fileName) {
        LOGGER.info(fileName);
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            SetupLineDto setupLineDto = mapper.readValue(inputStream,new TypeReference<SetupLineDto>(){});

            boolean saveLine = false;
            boolean saveUSer = false;

            Line line = null;
            Optional<Line> _line = lineRepository.findByLine(setupLineDto.getLine());
            if(!_line.isPresent()){

                line = new Line();
                List<LineStop> stops = getLineStopList(line, setupLineDto.getPlaces());
                List<LineSchedule> schedules = getFullYearSchedule(line);
                line.setLine(setupLineDto.getLine());
                line.setStops(stops);
                line.setLineSchedules(schedules);
                saveLine = true;

            } else {
                line = _line.get();
            }

            Optional<UserEntity> _user = this.userRepository.findUserEntityByEmail(setupLineDto.getAdmin().getEmail());
            UserEntity lineAdministrator = null;

            if(!_user.isPresent()) {

                lineAdministrator = getUser(setupLineDto.getAdmin(), UserRole.LINE_ADMINISTRATOR, line);
                String confirmationLink = confirmationUrl + "/confirm/" + lineAdministrator.getUuid();
                String template = String.format("Ciao %s,\n" +
                                "conferma il tuo account pedibus!\n\n" +
                                "%s\n\n" +
                                "La tua email per accedere è %s \n" +
                                "La tua password per accedere è %s \n\n" +
                                "Hai tempo 24 ore dopo le quali dovrai richiedere una nuova registrazione.\n\n" +
                                "Cordialmente,\n" +
                                "Il Team Pedibus",
                        lineAdministrator.getName(),confirmationLink,lineAdministrator.getEmail(),password);
                mailService.sendMail(lineAdministrator.getEmail(),
                        "Attivazione Account Pedibus",
                        template);
                LOGGER.info("register: confirmationLink:" + confirmationLink
                        + " expires at " + lineAdministrator.getRegistrationTime()
                        + " email: " + lineAdministrator.getEmail()
                        + " password: " + password);
                saveUSer = true;


            } else {

                final Line iLine = line;
                lineAdministrator = _user.get();
                Set<Role> roles = lineAdministrator.getRoles();
                if(roles.stream().noneMatch(role -> role.getLine().getLine().equals(iLine.getLine()) &&
                        role.getUserRole().toString().equals(UserRole.LINE_ADMINISTRATOR.toString()))) {
                    roles.add(Role.
                            builder().
                            userEntity(lineAdministrator).
                            userRole(UserRole.LINE_ADMINISTRATOR).
                            line(line).
                            build());
                    this.mailService.sendMail(lineAdministrator.getEmail(), "Amministrazione Linea",
                            "Gentile "+lineAdministrator.getName()+" "+lineAdministrator.getSurname()+"\n" +
                                    "le comunichiamo che è diventato amministratore della linea "+line.getLine()+" " +
                                    "su Pedibus.\n\n" +
                                    "Cordialmente," +
                                    "Il team Pedibus");
                    saveUSer = true;
                }
            }

            if(saveLine){
                this.lineRepository.save(line);
            }
            if(saveUSer){
                this.userRepository.save(lineAdministrator);
            }

        } catch (IOException e){
            LOGGER.error("Unable to load the line. Filename: " + fileName);
            e.printStackTrace();
        }
    }

    private List<LineStop> getLineStopList(Line line, List<SetupStopLineDto> places) {
        return places.stream().map(sl-> LineStop.builder().
                place(sl.getPlace()).
                line(line).
                toSchool(sl.getToSchool()).
                toHome(sl.getToHome()).
                lat(sl.getLat()).
                lon(sl.getLon()).
                build())
                .collect(Collectors.toList());
    }


    private List<LineSchedule> getFullYearSchedule(Line line){
        List<LineSchedule> lineYearSchedule = new ArrayList<>();
        for(int i =0; i < 20; i++ ){
            LineSchedule lineSchedule = new LineSchedule();
            lineSchedule.setLine(line);
            lineSchedule.setDay(LocalDate.now().plusDays(i));
            lineYearSchedule.add(lineSchedule);
        }
        return lineYearSchedule;
    }
    private UserEntity getUser(SetupUserDto userDto, UserRole userRole, Line line){
        Role role = new Role();
        HashSet<Role> roles =new HashSet<>();
        roles.add(role);

        long expireRegistrationTime = LocalDateTime.now().plusDays(1).atZone(ZoneOffset.UTC).toEpochSecond();
        UserEntity user = UserEntity.builder()
                .email(userDto.getEmail())
                .name(userDto.getName())
                .surname(userDto.getSurname())
                .password(passwordEncoder.encode(password))
                .enabled(false)
                .uuid(UUID.randomUUID().toString())
                .registrationTime(expireRegistrationTime)
                .roles(roles)
                .build();

        role.setUserEntity(user);
        role.setLine(line);
        role.setUserRole(userRole);

        return user;
    }
}
