package it.chickenfries.lab.setup;

import it.chickenfries.lab.dto.children.RegisterChildRequestDto;
import it.chickenfries.lab.model.DirectionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SetupReservationDto {

    private List<RegisterChildRequestDto> children;
    private String onBoard;
    private String offBoard;
    private DirectionType direction;
    private SetupUserDto parent;
    private String line;
}
