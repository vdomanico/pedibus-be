package it.chickenfries.lab.restcontroller;

import it.chickenfries.lab.dto.children.RegisterChildRequestDto;
import it.chickenfries.lab.dto.user.*;
import it.chickenfries.lab.model.Notification;
import it.chickenfries.lab.service.NotificationService;
import it.chickenfries.lab.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
public class UserRestController{

    private UserService userService;
    private NotificationService notificationService;

    @Autowired
    public UserRestController(UserService userService, NotificationService notificationService) {
        this.userService = userService;
        this.notificationService = notificationService;
    }

    @PostMapping(value = "/login")
    @ResponseStatus(OK)
    public UserAuthResponseDto login(@RequestBody @Valid LoginRequestDto loginRequestDto) {
        return userService.login(loginRequestDto.getEmail(), loginRequestDto.getPassword());
    }
    @PostMapping(value = "/register")
    @ResponseStatus(OK)
    @PreAuthorize("@permissionEvaluator.isLineAdministrator(#registerUserRequestDto.line)")
    public void registerAccount(@RequestBody @Valid RegisterUserRequestDto registerUserRequestDto) {
        userService.register(
                registerUserRequestDto.getEmail(),
                registerUserRequestDto.getPwd(),
                registerUserRequestDto.getName(),
                registerUserRequestDto.getSurname(),
                registerUserRequestDto.getRoles(),
                registerUserRequestDto.getLine());
    }

    @GetMapping(value = "/confirm/{randomUUID}")
    @ResponseStatus(OK)
    public void confirmAccountRegistration(@PathVariable(value = "randomUUID") String randomUUID) {
        userService.confirmRegistration(randomUUID);
    }

    @PostMapping(value = "/recover")
    @ResponseStatus(OK)
    public void getMailForRecoveryPassword(@RequestBody MailRequestDto mailRequestDto) {
        userService.recoverPwd(mailRequestDto.getEmail());
    }

    @GetMapping(value = {"/users", "/users/{line}"})
    @ResponseStatus(OK)
    @PreAuthorize("@permissionEvaluator.isAdministrator()")
    public List<UserResponseDto> getUsers(@PathVariable(value = "line", required = false) String line) {
        return userService.getUsers(line);
    }

    @PreAuthorize("@permissionEvaluator.isLineAdministrator(#lineUserGrantRequestDto.line)")
    @PutMapping(value = "/user/grant")
    public void addRemoveUserGrant(@RequestBody @Valid LineUserGrantRequestDto lineUserGrantRequestDto) {
        userService.addRemoveUserGrant(lineUserGrantRequestDto.isEnable(), lineUserGrantRequestDto.getLine(), lineUserGrantRequestDto.getEmail(), lineUserGrantRequestDto.getRole());
    }

    @PreAuthorize("@permissionEvaluator.isEmailOwner(#registerChildRequestDto.email)")
    @PostMapping(value = "/user/child")
    public ResponseEntity<Void> registerChild(@RequestBody @Valid RegisterChildRequestDto registerChildRequestDto){
        userService.registerChild(registerChildRequestDto.getName().trim(),
                registerChildRequestDto.getSurname().trim().toUpperCase(),
                registerChildRequestDto.getEmail().trim());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/user/info")
    public UserAuthResponseDto getUserInfo(){
        return this.userService.getUserInfo();
    }

    @PutMapping(value = "/user")
    @ResponseStatus(OK)
    public void updatePassword(@RequestBody @Valid UpdatePasswordRequestDto updatePasswordRequestDto) {
        userService.updatePassword(updatePasswordRequestDto.getCurrentPassword(), updatePasswordRequestDto.getNewPassword());
    }

    @GetMapping("/wstest")
    public void testController(){
        this.notificationService.sendNotification("mario.rossi@pedibus.com", Notification.builder().build());
    }

    @PostMapping(value = "/user/registrationsToken")
    @ResponseStatus(OK)
    public void addRegistrationToken(@RequestBody @NotBlank String registrationToken) {
        userService.addRegistrationToken(registrationToken);
    }
}
