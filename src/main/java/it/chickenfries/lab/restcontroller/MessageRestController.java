package it.chickenfries.lab.restcontroller;

import it.chickenfries.lab.persistency.entity.Message;
import it.chickenfries.lab.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
public class MessageRestController {

    private final MessageService messageService;

    @Autowired
    public MessageRestController(MessageService messageService) {
        this.messageService = messageService;
    }


    @GetMapping(value = "/messages")
    public Page<Message> getMessages(@NotNull Pageable pageable){
        return this.messageService.getMessages(pageable);
    }


    @GetMapping(value = "/messages/{id}")
    public void readMessage(@PathVariable(value = "id") String id) {
        this.messageService.readMessage(id);
    }

}
