package it.chickenfries.lab.restcontroller;

import it.chickenfries.lab.dto.line.LineResponseDto;
import it.chickenfries.lab.dto.line.LinesResponseDto;
import it.chickenfries.lab.service.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;

@RestController
public class LineRestController {

    private final LineService lineService;

    @Autowired
    public LineRestController(LineService lineService) {
        this.lineService = lineService;
    }

    @GetMapping("/lines")
    @ResponseStatus(OK)
    public LinesResponseDto getLines(){
        return lineService.getLines();
    }


    @GetMapping("/lines/{line}")
    @ResponseStatus(OK)
    @ResponseBody public LineResponseDto getLineDetail(@PathVariable("line") String line){
        return lineService.getLine(line);
    }



}
