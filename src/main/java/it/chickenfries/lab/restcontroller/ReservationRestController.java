package it.chickenfries.lab.restcontroller;

import it.chickenfries.lab.dto.children.ChildCollectRequestDto;
import it.chickenfries.lab.dto.children.ChildDeliverRequestDto;
import it.chickenfries.lab.dto.children.ChildResponseDto;
import it.chickenfries.lab.dto.reservation.*;
import it.chickenfries.lab.service.ReservationService;
import it.chickenfries.lab.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

import static org.springframework.http.HttpStatus.OK;

@RestController
public class ReservationRestController {

    private final ReservationService reservationService;

    @Autowired
    public ReservationRestController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PreAuthorize("@permissionEvaluator.isLineAdministrator(#line) or " +
            "@permissionEvaluator.isLineChaperon(#line)")
    @GetMapping(value = "/reservations/{line}/{date}")
    @ResponseStatus(OK)
    public LineReservationResponseDto getLineReservations(
            @PathVariable("line") String line,
            @PathVariable("date") String date){
        return reservationService.getLineReservations(line,DateUtil.getDateFromPathParam(date));
    }

    @PreAuthorize("@permissionEvaluator.isParentGranted(#line)")
    @GetMapping(value = "/reservations/parent/{line}/{date}")
    @ResponseStatus(OK)
    public LineReservationResponseDto getParentLineDayScheduleReservation(
            @PathVariable("line") String line,
            @PathVariable("date") String date){
        return reservationService.getParentLineDayScheduleReservation(
                line,
                DateUtil.getDateFromPathParam(date));
    }

    @PreAuthorize("@permissionEvaluator.isLineAdministrator(#line) or " +
            "@permissionEvaluator.isParentOfChildren(#reservationRequestDto.children)")
    @PostMapping(value = "/reservations/{line}/{date}")
    public ResponseEntity<ReservationTicketResponseDto> addReservation(
            @PathVariable("line") String line,
            @PathVariable("date") String date,
            @RequestBody @Valid ReservationRequestDto reservationRequestDto){
        Map<String, ChildResponseDto> reservations = reservationService.addReservation(
                line,
                DateUtil.getDateFromPathParam(date),
                reservationRequestDto);
        return new ResponseEntity<>(new ReservationTicketResponseDto(reservations), HttpStatus.CREATED);
    }

    @PreAuthorize(
            "@permissionEvaluator.isLineAdministrator(#line) or " +
                    "@permissionEvaluator.isLineChaperon(#line) or " +
                    "@permissionEvaluator.isReservationParentOwner(#reservationId)")
    @GetMapping(value = "/reservations/{line}/{date}/{reservationId}")
    @ResponseStatus(OK)
    public ReservationDetailResponseDto getReservation(@PathVariable("line") String line,
                                                       @PathVariable("date") String date,
                                                       @PathVariable("reservationId") String reservationId
                                                   ){
        return reservationService.getReservation(
                line,
                DateUtil.getDateFromPathParam(date),
                reservationId);
    }

    @PreAuthorize(
            "@permissionEvaluator.isLineAdministrator(#line) or " +
                    "@permissionEvaluator.isLineChaperon(#line) or " +
                    "@permissionEvaluator.isReservationParentOwner(#reservationUpdateRequestDto.ticket)")
    @PutMapping(value = "/reservations/{line}/{date}")
    public ResponseEntity<Void> updateReservation(@PathVariable("line") String line,
                                                    @PathVariable("date") String date,
                                                    @RequestBody @Valid ReservationUpdateRequestDto reservationUpdateRequestDto){
        reservationService.updateReservation(
                line,
                DateUtil.getDateFromPathParam(date),
                reservationUpdateRequestDto.getTicket(),
                reservationUpdateRequestDto);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize(
            "@permissionEvaluator.isLineAdministrator(#line) or " +
                    "@permissionEvaluator.isLineChaperon(#line) or " +
                    "@permissionEvaluator.isReservationParentOwner(#ticket)")
    @DeleteMapping(value = "/reservations/{line}/{ticket}")
    public ResponseEntity<Void> deleteReservation(@PathVariable("line") String line,
                                      @PathVariable("ticket") String ticket){
        reservationService.deleteReservation(ticket);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("@permissionEvaluator.isLineAdministrator(#line) or " +
            "@permissionEvaluator.isLineChaperon(#line)")
    @PostMapping(value = "/reservation/collect/{line}/{date}")
    public ResponseEntity<Void> collectChild(@PathVariable("line") String line,
                                             @PathVariable("date") String date,
                                             @RequestBody @Valid ChildCollectRequestDto childCollectRequestDto){
        reservationService.collectChild(
                childCollectRequestDto.getTicket(),
                childCollectRequestDto.isCollect());
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("@permissionEvaluator.isLineAdministrator(#line) or " +
            "@permissionEvaluator.isLineChaperon(#line)")
    @PostMapping(value = "/reservation/deliver/{line}/{date}")
    public ResponseEntity<Void> deliverChild(@PathVariable("line") String line,
                                             @PathVariable("date") String date,
                                             @RequestBody @Valid ChildDeliverRequestDto childDeliverRequestDto){
        reservationService.deliverChild(
                childDeliverRequestDto.getTicket(),
                childDeliverRequestDto.isDeliver());
        return ResponseEntity.ok().build();
    }



}
