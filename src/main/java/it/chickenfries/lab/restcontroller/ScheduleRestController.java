package it.chickenfries.lab.restcontroller;

import it.chickenfries.lab.dto.line.LineDayScheduleResponseDto;
import it.chickenfries.lab.dto.schedule.*;
import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.service.ScheduleService;
import it.chickenfries.lab.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

import static org.springframework.http.HttpStatus.OK;

@RestController
public class ScheduleRestController {

    private final ScheduleService scheduleService;

    @Autowired
    public ScheduleRestController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    /*
    retrieving line calendar
     */
    @PreAuthorize("@permissionEvaluator.isLineAdministrator(#line) or" +
            "@permissionEvaluator.isLineChaperon(#line) or" +
            "@permissionEvaluator.isParentGranted(#line)")
    @GetMapping("schedule/calendar/{line}")
    @ResponseStatus(OK)
    @ResponseBody
    public LineScheduleResponseDto getLineScheduleCalendar(
            @PathVariable("line") String line){
        return scheduleService.getLineSchedules(line);
    }

    /*
    retrieving line shifts calendar for chaperon
    */
    @PreAuthorize("@permissionEvaluator.isLineChaperon(#line)")
    @GetMapping("shifts/calendar/{line}")
    @ResponseStatus(OK)
    @ResponseBody
    public LineShiftResponseDto getLineShiftsCalendar(
            @PathVariable("line") String line){
        return scheduleService.getLineShifts(line);
    }

    /*
    retrieving line calendar - all chaperons
     */
    @PreAuthorize("@permissionEvaluator.isLineAdministrator(#line)")
    @GetMapping("schedule/{line}/{date}")
    @ResponseStatus(OK)
    @ResponseBody
    public LineDayScheduleResponseDto getLineSchedule(
            @PathVariable("line") String line,
            @PathVariable("date") String date){
        return scheduleService.getLineDaySchedule(line, DateUtil.getDateFromPathParam(date));
    }

    @PreAuthorize("@permissionEvaluator.isLineAdministrator(#addRemoveShiftDto.line)")
    @PostMapping("schedule/shifts")
    @ResponseStatus(OK)
    public void addRemoveShift(@RequestBody AddRemoveShiftDto addRemoveShiftDto){
        String line = addRemoveShiftDto.getLine();
        LocalDate date = DateUtil.getDateFromPathParam(addRemoveShiftDto.getDay());
        String email = addRemoveShiftDto.getEmail();
        DirectionType directionType = addRemoveShiftDto.getDirection();
        boolean addShift = addRemoveShiftDto.getAdd();
        this.scheduleService.addRemoveShift(line, date, email, directionType, addShift);
    }

    @PreAuthorize("@permissionEvaluator.isLineChaperon(#addRemoveAvailabilityDto.line)")
    @PostMapping("schedule/availabilities")
    @ResponseStatus(OK)
    public void addRemoveAvailability(@RequestBody AddRemoveAvailabilityDto addRemoveAvailabilityDto){
        this.scheduleService.addRemoveAvailability(addRemoveAvailabilityDto.getLine(),
                DateUtil.getDateFromPathParam(addRemoveAvailabilityDto.getDay()),
                addRemoveAvailabilityDto.getEmail(),
                addRemoveAvailabilityDto.getDirection(),
                addRemoveAvailabilityDto.getAdd());
    }

    /*
    retrieving line calendar - current chaperon logged
 */
    @PreAuthorize("@permissionEvaluator.isLineChaperon(#line)")
    @GetMapping("schedule/user/{line}/{date}")
    @ResponseStatus(OK)
    @ResponseBody
    public UserLineScheduleStatusResponseDto getUserLineSchedule(
            @PathVariable("line") String line,
            @PathVariable("date") String date){
        return scheduleService.getUserLineDaySchedule(line, DateUtil.getDateFromPathParam(date));
    }

}
