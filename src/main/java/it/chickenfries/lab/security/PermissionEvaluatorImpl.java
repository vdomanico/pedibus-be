package it.chickenfries.lab.security;

import it.chickenfries.lab.dto.children.ReservationChildRequestDto;
import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.model.exception.ReservationNotFoundException;
import it.chickenfries.lab.persistency.entity.Reservation;
import it.chickenfries.lab.persistency.entity.Role;
import it.chickenfries.lab.repository.ReservationRepository;
import it.chickenfries.lab.repository.RoleRepository;
import it.chickenfries.lab.service.ContextService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Component(value="permissionEvaluator")
public class PermissionEvaluatorImpl implements PermissionEvaluator {

    private final ContextService contextService;
    private Logger log = LogManager.getLogger(PermissionEvaluatorImpl.class);
    private final RoleRepository roleRepository;
    private final ReservationRepository reservationRepository;

    @Autowired
    public PermissionEvaluatorImpl(
            ContextService contextService,
            ReservationRepository reservationRepository,
            RoleRepository roleRepository){
        this.contextService = contextService;
        this.reservationRepository = reservationRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        log.debug("******************************************!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        log.debug("******************************************!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        return false;
    }

    @Transactional
    public boolean isLineAdministrator(String line) {
        String principalMail = contextService.getPrincipalMail();
        Optional<Role> role = this.roleRepository.findByUserEntity_EmailAndUserEntity_EnabledAndUserRoleAndLine_Line(principalMail,
                true,
                UserRole.LINE_ADMINISTRATOR,
                line);
        return role.isPresent();
    }

    @Transactional
    public boolean isAdministrator() {
        String principalMail = contextService.getPrincipalMail();
        List<Role> roles = this.roleRepository.findByUserEntity_EmailAndUserEntity_EnabledAndUserRole(principalMail,
                true,
                UserRole.LINE_ADMINISTRATOR);
        return roles != null && !roles.isEmpty();
    }

    @Transactional
    public boolean isLineChaperon(String line) {
        String principalMail = contextService.getPrincipalMail();
        Optional<Role> role = this.roleRepository.findByUserEntity_EmailAndUserEntity_EnabledAndUserRoleAndLine_Line(
                principalMail,
                true,
                UserRole.CHAPERON,
                line);
        return role.isPresent();
    }

    @Transactional
    public boolean isParentGranted(String line) {
        String principalMail = contextService.getPrincipalMail();
        Optional<Role> role = this.roleRepository.findByUserEntity_EmailAndUserEntity_EnabledAndUserRoleAndLine_Line(
                principalMail,
                true,
                UserRole.PARENT,
                line);
        return role.isPresent();
    }

    @Transactional
    public boolean isEmailOwner(String email) {
        String principalMail = contextService.getPrincipalMail();
        return principalMail.equals(email);
    }

    public boolean isParentOfChildren(List<ReservationChildRequestDto> children){
        return children.stream().allMatch(childDto -> isEmailOwner(childDto.getEmail()));
    }

    @Transactional
    public boolean isReservationParentOwner(String reservationId) {
        Optional<Reservation> optRes = reservationRepository.findByTicket(reservationId);

        if(!optRes.isPresent()){
            throw new ReservationNotFoundException();
        }
        Reservation reservation = optRes.get();

        return reservation.getUserEntity().getEmail().equals(contextService.getPrincipalMail());
    }
}
