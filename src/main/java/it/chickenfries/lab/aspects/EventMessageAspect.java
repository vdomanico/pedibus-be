package it.chickenfries.lab.aspects;

import it.chickenfries.lab.dto.reservation.ReservationRequestDto;
import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.persistency.entity.Message;
import it.chickenfries.lab.persistency.entity.UserEntity;
import it.chickenfries.lab.service.*;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Aspect
@Component
public class EventMessageAspect {

    private final MailService mailService;
    private final MessageService messageService;
    private final NotificationService notificationService;
    private final PushNotificationService pushNotificationService;
    private final ContextService contextService;

    @Autowired
    public EventMessageAspect(MailService mailService,
                              MessageService messageService,
                              NotificationService notificationService,
                              PushNotificationService pushNotificationService, ContextService contextService) {
        this.mailService = mailService;
        this.messageService = messageService;
        this.notificationService = notificationService;
        this.pushNotificationService = pushNotificationService;
        this.contextService = contextService;
    }

    @AfterReturning(value = "it.chickenfries.lab.aspects.CommonJoinPointConfig.addRemoveShiftExecution() && args(line, date, email, direction, add)",
            argNames = "joinPoint,line,date,email,direction,add")
    void addRemoveShiftEvent(JoinPoint joinPoint, String line, LocalDate date, String email, DirectionType direction, Boolean add){
        UserEntity user = this.getUserEntity(email);
        this.messageService.addRemoveShiftSendMessage(line, date, email, direction, add);
        this.notificationService.addRemoveShiftSendNotification(line, date, email, direction, add);
        this.pushNotificationService.addRemoveShiftSendNotification(line, date, user, email, direction, add);
    }

    @AfterReturning(value = "it.chickenfries.lab.aspects.CommonJoinPointConfig.addRemoveAvailabilityExecution() && args(line,day,email,direction,add)",
            argNames = "joinPoint,line,day,email,direction,add")
    void addRemoveAvailabilityEvent(JoinPoint joinPoint, String line, LocalDate day, String email, DirectionType direction, Boolean add){
        this.messageService.addRemoveAvailabilitySendMessage(line,day,email,direction,add);
        this.notificationService.addRemoveAvailabilitySendNotification(line,day,email,direction,add);
    }

    @AfterReturning(value = "it.chickenfries.lab.aspects.CommonJoinPointConfig.addReservationExecution() && args(line, date, reservationRequestDto)",
            argNames = "joinPoint,line,date,reservationRequestDto")
    void addReservationEvent(JoinPoint joinPoint, String line, LocalDate date, ReservationRequestDto reservationRequestDto) {
        this.messageService.addReservationSendMessage(line, date, reservationRequestDto);
        this.notificationService.addReservationSendNotification(line,date,reservationRequestDto);
    }

    @AfterReturning(value = "it.chickenfries.lab.aspects.CommonJoinPointConfig.deleteReservationExecution() && args(ticket)",
            argNames = "joinPoint,ticket")
    void deleteReservationEvent(JoinPoint joinPoint, String ticket) {
        this.messageService.deleteReservationSendMessage(ticket);
        this.notificationService.deleteReservationSendNotification(ticket);
    }

    @AfterReturning(value = "it.chickenfries.lab.aspects.CommonJoinPointConfig.addRemoveUserGrantExecution() && args(enable, line, email, userRole)",
            argNames = "joinPoint, enable, line, email, userRole")
    void addRemoveRoleEvent(JoinPoint joinPoint, boolean enable, String line, String email, UserRole userRole) {
        UserEntity user = this.getUserEntity(email);
        this.messageService.addRemoveRoleSendMessage(enable, line, email, userRole);
        this.notificationService.addRemoveRoleSendNotification(enable, line, email, userRole);
        this.pushNotificationService.addRemoveRoleSendNotification(enable, line, user, email, userRole);
    }

    @AfterReturning(value = "it.chickenfries.lab.aspects.CommonJoinPointConfig.collectChildExecution() && args(ticket, collect)",
            argNames = "joinPoint, ticket, collect")
    void collectChildEvent(JoinPoint joinPoint, String ticket, boolean collect) {
        this.mailService.collectChildSendMail(ticket, collect);
        this.messageService.collectChildSendMessage(ticket, collect);
        this.notificationService.collectChildSendNotification(ticket, collect);
    }

    @AfterReturning(value = "it.chickenfries.lab.aspects.CommonJoinPointConfig.deliverChildExecution() && args(ticket, deliver)",
            argNames = "joinPoint, ticket, deliver")
    void deliverChildEvent(JoinPoint joinPoint, String ticket, boolean deliver) {
        this.mailService.deliverChildSendMail(ticket, deliver);
        this.messageService.deliverChildSendMessage(ticket, deliver);
        this.notificationService.deliverChildSendNotification(ticket, deliver);
    }

    @AfterReturning(value = "it.chickenfries.lab.aspects.CommonJoinPointConfig.saveMessageExecution() && args(message)",
            argNames = "joinPoint, message")
    void saveMessageEvent(JoinPoint joinPoint, Message message) {
        this.notificationService.notifyUnreadMessages(message.getUserEntity().getEmail());
    }

    private UserEntity getUserEntity(String email) {
        return this.contextService.getUserByEmail(email);
    }

}
