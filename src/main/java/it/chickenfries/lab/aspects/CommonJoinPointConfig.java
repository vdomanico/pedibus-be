package it.chickenfries.lab.aspects;

import org.aspectj.lang.annotation.Pointcut;

public class CommonJoinPointConfig {

    @Pointcut("execution(* it.chickenfries.lab.service.ScheduleService.addRemoveShift(..))")
    public void addRemoveShiftExecution(){ }

    @Pointcut("execution(* it.chickenfries.lab.service.ScheduleService.addRemoveAvailability(..))")
    public void addRemoveAvailabilityExecution(){ }

    @Pointcut("execution(* it.chickenfries.lab.service.ReservationService.addReservation(..))")
    public void addReservationExecution(){}

    @Pointcut("execution(* it.chickenfries.lab.service.ReservationService.deleteReservation(..))")
    public void deleteReservationExecution(){}

    @Pointcut("execution(* it.chickenfries.lab.service.UserService.addRemoveUserGrant(..))")
    public void addRemoveUserGrantExecution(){}

    @Pointcut("execution(* it.chickenfries.lab.service.ReservationService.collectChild(..))")
    public void collectChildExecution(){}

    @Pointcut("execution(* it.chickenfries.lab.service.ReservationService.deliverChild(..))")
    public void deliverChildExecution(){}

    @Pointcut("execution(* it.chickenfries.lab.repository.MessageRepository.save(..))")
    public void saveMessageExecution() {
    }

}
