package it.chickenfries.lab.service;

import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.persistency.entity.Role;
import it.chickenfries.lab.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(
            ContextService contextService,
            RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Optional<Role> getOptionalRole(String email, String line, UserRole userRole){
        return this.roleRepository.
                findByUserEntity_EmailAndUserEntity_EnabledAndUserRoleAndLine_Line(
                        email,
                        true,
                        userRole,
                        line);
    }
}
