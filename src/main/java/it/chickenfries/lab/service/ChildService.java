package it.chickenfries.lab.service;

import it.chickenfries.lab.model.exception.ChildNotFoundException;
import it.chickenfries.lab.persistency.entity.Child;
import it.chickenfries.lab.repository.ChildRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ChildService {

    private final ChildRepository childRepository;

    @Autowired
    public ChildService(ChildRepository childRepository) {
        this.childRepository = childRepository;
    }

    public Child getChildWithException(String email, String name, String surname){
        Optional<Child> optionalChild = childRepository.findByUserEntity_EmailAndName_AndSurname(email, name, surname);
        if(!optionalChild.isPresent()){
            throw new ChildNotFoundException();
        }
        return optionalChild.get();
    }

}
