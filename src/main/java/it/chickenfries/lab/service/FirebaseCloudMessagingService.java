package it.chickenfries.lab.service;

import com.google.firebase.messaging.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FirebaseCloudMessagingService {

    private final Logger logger = LoggerFactory.getLogger(FirebaseCloudMessagingService.class);

    public void sendNotification(List<String> registrationTokens,
                                 String title,
                                 String body) throws FirebaseMessagingException {

        if(registrationTokens == null || registrationTokens.isEmpty()){
            return;
        }

        MulticastMessage message = MulticastMessage.builder()
                .setNotification(new Notification(
                        title, body
                ))
                .addAllTokens(registrationTokens)
                .build();
        BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(message);

        logger.info("push notification sent to " + registrationTokens);
    }

}
