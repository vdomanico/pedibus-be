package it.chickenfries.lab.service;

import it.chickenfries.lab.dto.line.LineDayScheduleResponseDto;
import it.chickenfries.lab.dto.schedule.*;
import it.chickenfries.lab.dto.user.UserResponseDto;
import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.model.exception.*;
import it.chickenfries.lab.persistency.entity.*;
import it.chickenfries.lab.repository.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ScheduleService {

    private static final Logger LOG = LogManager.getLogger(ScheduleService.class.getCanonicalName());
    private final LineScheduleRepository lineScheduleRepository;
    private final LineRepository lineRepository;
    private final ShiftRepository shiftRepository;
    private final AvailabilityRepository availabilityRepository;
    private final UserRepository userRepository;
    private final ContextService contextService;

    @Autowired
    public ScheduleService(ContextService contextService,
            LineScheduleRepository lineScheduleRepository,
                           LineRepository lineRepository,
                           UserRepository userRepository,
                           ShiftRepository shiftRepository,
                           AvailabilityRepository availabilityRepository) {
        this.contextService = contextService;
        this.lineScheduleRepository = lineScheduleRepository;
        this.shiftRepository = shiftRepository;
        this.availabilityRepository = availabilityRepository;
        this.lineRepository = lineRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public LineDayScheduleResponseDto getLineDaySchedule(String line, LocalDate date) {
        LineSchedule lineSchedule = this.getLineScheduleWithException(line, date);

        LineDayScheduleResponseDto lineDayScheduleResponseDto = new LineDayScheduleResponseDto();
        List<UserLineScheduleStatusResponseDto> users = new ArrayList<>();
        lineDayScheduleResponseDto.setLine(line);
        lineDayScheduleResponseDto.setDay(date);
        lineDayScheduleResponseDto.setUsers(users);

        //retrieve all chaperons (only those who confirmed their account)
        List<UserEntity> allLineChaperons = lineSchedule.getLine().
                getRoles().stream().
                filter(role -> role.getUserRole().toString().equals(UserRole.CHAPERON.toString()) &&
                        role.getUserEntity().isEnabled()).
                map(Role::getUserEntity).collect(Collectors.toList());

        allLineChaperons.forEach(user ->  {
            //check if the chaperon has already one shift for another line
            Shift alreadyReservedToHomeShift = user.getShifts().stream().
                    filter(shift -> shift.getLineSchedule().getDay().equals(date) &&
                            shift.getDirection().toString().equals(DirectionType.TO_HOME.toString()) &&
                            !shift.getLineSchedule().getLine().getLine().equals(line)).findFirst().orElse(null);
            String toHomeLineElseWhere = alreadyReservedToHomeShift != null ? alreadyReservedToHomeShift.getLineSchedule().getLine().getLine() : null;
            Shift alreadyReservedToSchoolShift = user.getShifts().stream().
                    filter(shift -> shift.getLineSchedule().getDay().equals(date) &&
                            shift.getDirection().toString().equals(DirectionType.TO_SCHOOL.toString()) &&
                            !shift.getLineSchedule().getLine().getLine().equals(line)).findFirst().orElse(null);
            String toSchoolLineElseWhere = alreadyReservedToSchoolShift != null ? alreadyReservedToSchoolShift.getLineSchedule().getLine().getLine() : null;
            //check if the chaperon has the shift for this line
            boolean reservedThisToHome = user.getShifts().stream().
                    anyMatch(shift -> shift.getLineSchedule().getDay().equals(date) &&
                            shift.getDirection().toString().equals(DirectionType.TO_HOME.toString()) &&
                            shift.getLineSchedule().getLine().getLine().equals(line));

            boolean reservedThisToSchool = user.getShifts().stream().
                    anyMatch(shift -> shift.getLineSchedule().getDay().equals(date) &&
                            shift.getDirection().toString().equals(DirectionType.TO_SCHOOL.toString()) &&
                            shift.getLineSchedule().getLine().getLine().equals(line));
            //check if the chaperon has given the availability for this shift
            boolean availableForThisShiftToHome = user.getAvailabilities().stream().
                    anyMatch(availability -> availability.getLineSchedule().getDay().equals(date) &&
                            availability.getDirection().toString().equals(DirectionType.TO_HOME.toString()) &&
                            availability.getLineSchedule().getLine().getLine().equals(line));

            boolean availableForThisShiftToSchool = user.getAvailabilities().stream().
                    anyMatch(availability -> availability.getLineSchedule().getDay().equals(date) &&
                            availability.getDirection().toString().equals(DirectionType.TO_SCHOOL.toString()) &&
                            availability.getLineSchedule().getLine().getLine().equals(line));

            UserLineScheduleStatusResponseDto userLineScheduleStatusResponseDto = UserLineScheduleStatusResponseDto.builder().
                    userDetail(UserResponseDto.builder().
                            email(user.getEmail()).
                            name(user.getName()).
                            surname(user.getSurname()).
                            build()).
                    to_home(UseStatusResponseDto.builder().
                            shift(reservedThisToHome).
                            available(availableForThisShiftToHome).
                            reservedElseWhere(alreadyReservedToHomeShift!=null).
                            lineElseWhere(toHomeLineElseWhere).
                            build()).
                    to_school(UseStatusResponseDto.builder().
                            shift(reservedThisToSchool).
                            available(availableForThisShiftToSchool).
                            reservedElseWhere(alreadyReservedToSchoolShift!=null).
                            lineElseWhere(toSchoolLineElseWhere).
                            build()).
                    build();
            users.add(userLineScheduleStatusResponseDto);
        });

        return lineDayScheduleResponseDto;
    }

    @Transactional
    public LineScheduleResponseDto getLineSchedules(String line) {
        List<LineSchedule> schedules = this.lineScheduleRepository.findAllByLine_line(line);
        return LineScheduleResponseDto.builder().
                line(line).
                days(schedules.stream().map(LineSchedule::getDay).collect(Collectors.toList())).
                build();
    }

    @Transactional
    public LineShiftResponseDto getLineShifts(String line) {
        LineShiftResponseDto lineShiftResponseDto = new LineShiftResponseDto();
        lineShiftResponseDto.setLine(line);

        UserEntity chaperon = this.contextService.getContextUserWithException();
        Collection<ShiftScheduleDto> shifts = chaperon.getShifts().stream().filter(shift -> shift.getLineSchedule().getLine().getLine().equals(line)).
                map(shift -> ShiftScheduleDto.builder().
                        line(line).
                        date(shift.getLineSchedule().getDay()).
                        toHome(shift.getDirection().equals(DirectionType.TO_HOME)).
                        toSchool(shift.getDirection().equals(DirectionType.TO_SCHOOL)).
                        build()).
                collect(Collectors.toMap(
                        ShiftScheduleDto::getDate, Function.identity(),
                        (ShiftScheduleDto o, ShiftScheduleDto o2) -> {
                            o.setToHome(true);
                            o.setToSchool(true);
                            return o;
                        })).values();

        List<ShiftScheduleDto> _shift = new ArrayList<>(shifts);
        _shift.sort(Comparator.comparing(ShiftScheduleDto::getDate));
        lineShiftResponseDto.setShifts(_shift);
        return lineShiftResponseDto;
    }

    @Transactional
    public void addRemoveShift(String line, LocalDate day, String email, DirectionType direction, Boolean add) {

        LineSchedule lineSchedule = this.getLineScheduleWithException(line, day);
        UserEntity user = getUserWithException(email);

        if(isPedAlreadyLeft(lineSchedule, day, direction)){
                throw new ShiftNotModifiableException();
        }

        if(add){

            LOG.info("adding shift - start");
            if (lineSchedule.getShifts().stream().anyMatch(
                    shift -> shift.getUserEntity().getEmail().equals(email) &&
                            shift.getDirection().toString().equals(direction.toString()))) {
                throw new ShiftConflictException();
            }

            Shift shift = new Shift(lineSchedule, user, direction);
            List<Shift> shifts = lineSchedule.getShifts();
            if (shifts == null) {
                shifts = new ArrayList<>();
            }
            shifts.add(shift);

            Line parentLine = lineSchedule.getLine();
            lineRepository.save(parentLine);
            LOG.info("adding shift - end");

        }

        else {

            LOG.info("removing shift - start");
            Optional<Shift> _shift = lineSchedule.getShifts().
                    stream().filter(shift -> shift.getUserEntity().getEmail().equals(email) &&
                    shift.getDirection().toString().equals(direction.toString())).findFirst();

            if (!_shift.isPresent()) {
                throw new ShiftNotFoundException();
            }

            Shift shift = _shift.get();
            /* Removing from LineSchedule - oneToMany..*/
            lineSchedule.getShifts().removeIf(
                    s -> s.equals(shift));

            /* Removing from User - oneToMany..*/
            UserEntity sUser = shift.getUserEntity();
            sUser.getShifts().removeIf(
                    s -> s.equals(shift));

            /*removing shift from db*/
            shiftRepository.delete(shift);

            LOG.info("removing shift - end");
        }

    }

    @Transactional
    public void addRemoveAvailability(String line, LocalDate day, String email, DirectionType direction, Boolean add) {
        LineSchedule lineSchedule = this.getLineScheduleWithException(line, day);
        UserEntity user = getUserWithException(email);

        if(isPedAlreadyLeft(lineSchedule, day, direction)){
            throw new AvailabilityNotModifiableException();
        }

        if(add) {
            LOG.info("adding availability - start");
            if(lineSchedule.getAvailabilities().stream().anyMatch(
                    availability -> availability.getUserEntity().getEmail().equals(email) &&
                            availability.getDirection().toString().equals(direction.toString()))){
                throw new AvailabilityConflictException();
            }

            Availability availability = new Availability(lineSchedule ,user, direction);
            List<Availability> availabilities = lineSchedule.getAvailabilities();
            if(availabilities == null){
                availabilities = new ArrayList<>();
            }
            availabilities.add(availability);

            Line parentLine = lineSchedule.getLine();
            lineRepository.save(parentLine);
            LOG.info("adding availability - end");
        }

        else{
            LOG.info("removing availability - start");
            Optional<Availability> _availability = lineSchedule.getAvailabilities().
                    stream().filter(availabilities -> availabilities.getUserEntity().getEmail().equals(email) &&
                    availabilities.getDirection().toString().equals(direction.toString())).findFirst();

            if (!_availability.isPresent()) {
                throw new AvailabilityNotFoundException();
            }

            Availability availability = _availability.get();

            Optional<Shift> optionalShift =
                    user.getShifts().stream()
                    .filter(shift -> shift.getLineSchedule().getDay().equals(day) &&
                            shift.getDirection().toString().equals(direction.toString()))
                    .findFirst();

            if (optionalShift.isPresent()){
                throw new AvailabilityNotModifiableException();
            }


            /* Removing from LineSchedule - oneToMany..*/
            lineSchedule.getAvailabilities().removeIf( a ->
                    a.getDirection().equals(direction) &&
                            a.getLineSchedule().getDay().equals(day) && a.getLineSchedule().getLine().getLine().equals(line));

            /* Removing from User - oneToMany..*/
            UserEntity sUser = availability.getUserEntity();
            sUser.getAvailabilities().removeIf( a ->
                    a.getDirection().equals(direction) &&
                            a.getLineSchedule().getDay().equals(day) && a.getLineSchedule().getLine().getLine().equals(line));

            /*removing availability from db*/
            availabilityRepository.delete(availability);

            LOG.info("removing availability - end");
        }

    }

    @Transactional
    public UserLineScheduleStatusResponseDto getUserLineDaySchedule(String line, LocalDate date) {
        LineSchedule lineSchedule = this.getLineScheduleWithException(line, date);

        String leggedUserEmail = this.contextService.getPrincipalMail();
        Optional<UserEntity> _chaperon = lineSchedule.getLine().
                getRoles().stream().
                filter(role -> role.getUserRole().toString().equals(UserRole.CHAPERON.toString()) &&
                        role.getUserEntity().getEmail().equals(leggedUserEmail)).
                map(Role::getUserEntity).findFirst();

        if(!_chaperon.isPresent()){
            throw new UserNotFoundException();
        }

        UserEntity chaperon = _chaperon.get();

        //check if the chaperon is available for this line schedule
        Optional<Availability> _avToHome = chaperon.getAvailabilities().stream().filter(av ->
                av.getLineSchedule().getDay().isEqual(date) &&
                        av.getLineSchedule().getLine().getLine().equals(line) &&
                av.getDirection().toString().equals(DirectionType.TO_HOME.toString())).findFirst();
        Optional<Availability> _avToSchool = chaperon.getAvailabilities().stream().filter(av ->
                av.getLineSchedule().getDay().isEqual(date) &&
                        av.getLineSchedule().getLine().getLine().equals(line) &&
                        av.getDirection().toString().equals(DirectionType.TO_SCHOOL.toString())).findFirst();

        //check if the chaperon has the shift for this line schedule
        Optional<Shift> _sToHome = chaperon.getShifts().stream().filter(s ->
                s.getLineSchedule().getDay().isEqual(date) &&
                        s.getLineSchedule().getLine().getLine().equals(line) &&
                        s.getDirection().toString().equals(DirectionType.TO_HOME.toString())).findFirst();
        Optional<Shift> _sToSchool = chaperon.getShifts().stream().filter(s ->
                s.getLineSchedule().getDay().isEqual(date) &&
                        s.getLineSchedule().getLine().getLine().equals(line) &&
                        s.getDirection().toString().equals(DirectionType.TO_SCHOOL.toString())).findFirst();

        //remind the user where he has the shift - if any
        Optional<Shift> _sToHomeWhere = chaperon.getShifts().stream().filter(s ->
                s.getLineSchedule().getDay().isEqual(date) &&
                        !s.getLineSchedule().getLine().getLine().equals(line) &&
                        s.getDirection().toString().equals(DirectionType.TO_HOME.toString())).findFirst();
        Optional<Shift> _sToSchoolWhere= chaperon.getShifts().stream().filter(s ->
                s.getLineSchedule().getDay().isEqual(date) &&
                        !s.getLineSchedule().getLine().getLine().equals(line) &&
                        s.getDirection().toString().equals(DirectionType.TO_SCHOOL.toString())).findFirst();
        String sToHomeWhere = null;
        String sToSchoolWhere = null;
        if(_sToHomeWhere.isPresent()){
            sToHomeWhere = _sToHomeWhere.get().getLineSchedule().getLine().getLine();
        }
        if(_sToSchoolWhere.isPresent()){
            sToSchoolWhere = _sToSchoolWhere.get().getLineSchedule().getLine().getLine();
        }
        return UserLineScheduleStatusResponseDto.builder().
                userDetail(UserResponseDto.builder().
                        email(chaperon.getEmail()).
                        name(chaperon.getName()).
                        surname(chaperon.getSurname()).
                        build()).
                to_home(UseStatusResponseDto.builder().
                        shift(_sToHome.isPresent()).
                        available(_avToHome.isPresent()).
                        reservedElseWhere(sToHomeWhere!=null).
                        lineElseWhere(sToHomeWhere).
                        build()).
                to_school(UseStatusResponseDto.builder().
                        shift(_sToSchool.isPresent()).
                        available(_avToSchool.isPresent()).
                        reservedElseWhere(sToSchoolWhere!=null).
                        lineElseWhere(sToSchoolWhere).
                        build()).
                build();
    }


    private LineSchedule getLineScheduleWithException(String line, LocalDate date){
        Optional<LineSchedule> _lS = this.lineScheduleRepository.findByDayAndLine_Line(date, line);
        if(!_lS.isPresent()){
            throw new LineScheduleNotFoundException();
        }
        return _lS.get();
    }

    private UserEntity getUserWithException(String email){
        Optional<UserEntity> _user = this.userRepository.findUserEntityByEmail(email);
        if(!_user.isPresent()){
            throw new UserNotFoundException();
        }
        return _user.get();
    }

    private boolean isPedAlreadyLeft(LineSchedule lineSchedule, LocalDate day, DirectionType direction){
        Optional<Time> departureTime =
                lineSchedule.getLine().getStops()
                        .stream()
                        .map(direction.equals(DirectionType.TO_SCHOOL) ? LineStop::getToSchool:  LineStop::getToHome)
                        .sorted().findFirst();

        if (!departureTime.isPresent()) {
            throw new LineNotFoundException();
        }
        return day.isBefore(LocalDate.now()) || day.isEqual(LocalDate.now()) && departureTime.get().toLocalTime().isBefore(LocalTime.now());
    }
}
