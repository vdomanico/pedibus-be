package it.chickenfries.lab.service;

import it.chickenfries.lab.dto.reservation.ReservationRequestDto;
import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.model.Notification;
import it.chickenfries.lab.model.NotificationType;
import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.model.exception.LineScheduleNotFoundException;
import it.chickenfries.lab.model.exception.ReservationNotFoundException;
import it.chickenfries.lab.model.exception.UserNotFoundException;
import it.chickenfries.lab.persistency.entity.Reservation;
import it.chickenfries.lab.persistency.entity.UserEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Service
public class NotificationService {

    private SimpMessagingTemplate messagingTemplate;
    private static final Logger LOG = LogManager.getLogger(NotificationService.class.getCanonicalName());
    private ContextService contextService;

    @Autowired
    public NotificationService(SimpMessagingTemplate messagingTemplate, ContextService contextService) {
        this.messagingTemplate = messagingTemplate;
        this.contextService = contextService;
    }

    public void sendNotification(String username, Notification notification){
        messagingTemplate.
                convertAndSendToUser(username,"/queue/notifications", notification);
    }


    public void addRemoveShiftSendNotification(String line,
                                               LocalDate date,
                                               String email,
                                               DirectionType direction,
                                               Boolean add){
        String templateMessage = add ?
                String.format("Nuovo turno il %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"))) :
                String.format("Rimosso turno il %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")));
        String subject = add ?
                String.format("Nuovo turno il %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"))) :
                String.format("Rimosso turno il %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")));

        this.sendNotification(email, Notification.builder().
                id(UUID.randomUUID().toString()).
                title(subject).
                message(templateMessage).
                type(NotificationType.INFO).
                build());

    }

    public void addRemoveRoleSendNotification(boolean enable, String line, String email, UserRole userRole) {
        if (this.contextService.getPrincipalMail().equals(email)) {
            return;
        }

        String subject =enable  ? "Nuovo ruolo" : "Rimosso ruolo";
        String templateMessage = enable ?
                String.format("Nuovo ruolo %s linea %s", userRole.toString(), line) :
                String.format("Rimosso ruolo %s linea %s", userRole.toString(), line);
        this.sendNotification(email, Notification.builder().
                id(UUID.randomUUID().toString()).
                title(subject).
                message(templateMessage).
                type(NotificationType.REFRESH).
                build());
    }

    public void addRemoveAvailabilitySendNotification(String line, LocalDate day, String email, DirectionType direction, Boolean add) {

        UserEntity user = contextService.getUserByEmail(email);

        String userNameSurname = user.getName()+" "+user.getSurname();

        String templateMessage = add ?
                String.format("Confermata disponibilità di %s il %s su linea %s", userNameSurname, day.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line) :
                String.format("Rimossa disponibilità di %s il %s su linea %s", userNameSurname, day.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);

        String subject = add ?
                String.format("Disponibilità di %s il %s", userNameSurname, day.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"))) :
                String.format("Disponibilità di %s il %s", userNameSurname, day.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")));

        List<UserEntity> administrators = null;

        try{
            administrators = contextService.getAdministrators(line);
        } catch (UserNotFoundException e){
            LOG.error(e.getMessage());
        }

        if (administrators != null) {

            for (UserEntity admin : administrators) {

                this.sendNotification(admin.getEmail(), Notification.builder().
                        id(UUID.randomUUID().toString()).
                        title(subject).
                        message(templateMessage).
                        type(NotificationType.INFO).
                        build());

            }
        }
    }

    public void addReservationSendNotification(String line, LocalDate date, ReservationRequestDto reservationRequestDto) {

        List<UserEntity> chaperons = null;

        try{
            chaperons = contextService.getChaperons(line, date, reservationRequestDto.getDirection());
        }catch (LineScheduleNotFoundException e){
            LOG.error(e.getMessage());
        }

        String templateMessage = String.format("Nuova prenotazione il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);
        String subject = String.format("Nuova prenotazione il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);

        if (chaperons != null) {
            for (UserEntity chaperon : chaperons) {

                this.sendNotification(chaperon.getEmail(), Notification.builder().
                        id(UUID.randomUUID().toString()).
                        title(subject).
                        message(templateMessage).
                        type(NotificationType.INFO).
                        build());
            }
        }
    }

    public void deleteReservationSendNotification(String ticket) {

        Reservation reservation;

        try{
            reservation = contextService.getReservationWithException(ticket);
        }catch (ReservationNotFoundException e){
            LOG.error(e.getMessage());
            return;
        }

        List<UserEntity> chaperons = null;
        String line = reservation.getLineSchedule().getLine().getLine();
        LocalDate date = reservation.getDate();
        DirectionType directionType = reservation.getDirection();


        try{
            chaperons = contextService.getChaperons(line, date, directionType);
        }catch (LineScheduleNotFoundException e){
            LOG.error(e.getMessage());
        }

        if (chaperons != null) {

            String templateMessage = String.format("Cancellata prenotazione del %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);
            String subject = String.format("Cancellata prenotazione del %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);

            for (UserEntity chaperon : chaperons) {

                this.sendNotification(chaperon.getEmail(), Notification.builder().
                        id(UUID.randomUUID().toString()).
                        title(subject).
                        message(templateMessage).
                        type(NotificationType.INFO).
                        build());

            }
        }
    }

    public void collectChildSendNotification(String ticket, boolean collect) {
        Reservation reservation;
        String chaperon;
        try{
            reservation = contextService.getReservationWithException(ticket);
            UserEntity user = this.contextService.getContextUser();
            chaperon = user.getName() + " " + user.getSurname();
        }catch (ReservationNotFoundException | UserNotFoundException e){
            LOG.error(e.getMessage());
            return;
        }
        if (collect) {

            String parentEmail = reservation.getUserEntity().getEmail();
            String place = reservation.getOn_B().getPlace();
            String child = reservation.getChild().getName();
            String title = String.format("%s è in partenza" , child);
            String templateMessage = String.format("L'accompagnatore %s ha recuperato %s presso %s", chaperon, child, place);
            this.sendNotification(parentEmail, Notification.builder().
                    id(UUID.randomUUID().toString()).
                    title(title).
                    message(templateMessage).
                    type(NotificationType.MONITOR).
                    build());

        }
    }

    public void deliverChildSendNotification(String ticket, boolean deliver) {
        Reservation reservation;
        String chaperon;
        try{
            reservation = contextService.getReservationWithException(ticket);
            UserEntity user = this.contextService.getContextUser();
            chaperon = user.getName() + " " + user.getSurname();
        }catch (ReservationNotFoundException | UserNotFoundException e){
            LOG.error(e.getMessage());
            return;
        }
        if (deliver) {

            String parentEmail = reservation.getUserEntity().getEmail();
            String place = reservation.getOn_B().getPlace();
            String child = reservation.getChild().getName();
            String title = String.format("%s è arrivato" , child);
            String templateMessage = String.format("L'accompagnatore %s ha consegnato %s presso %s", chaperon, child, place);
            this.sendNotification(parentEmail, Notification.builder().
                    id(UUID.randomUUID().toString()).
                    title(title).
                    message(templateMessage).
                    type(NotificationType.MONITOR).
                    build());

        }
    }

    public void notifyUnreadMessages(String email) {
        int unreadMessages = this.contextService.getUnreadMessages(email);
        this.sendNotification(email, Notification.builder().
                id(UUID.randomUUID().toString()).
                title("Unread Messages").
                message(Integer.toString(unreadMessages)).
                type(NotificationType.UNREAD_MESSAGES).
                build());
    }
}
