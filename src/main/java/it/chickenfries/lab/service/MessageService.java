package it.chickenfries.lab.service;

import it.chickenfries.lab.dto.reservation.ReservationRequestDto;
import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.model.exception.LineScheduleNotFoundException;
import it.chickenfries.lab.model.exception.MessageNotFoundException;
import it.chickenfries.lab.model.exception.ReservationNotFoundException;
import it.chickenfries.lab.model.exception.UserNotFoundException;
import it.chickenfries.lab.persistency.entity.Message;
import it.chickenfries.lab.persistency.entity.Reservation;
import it.chickenfries.lab.persistency.entity.UserEntity;
import it.chickenfries.lab.repository.MessageRepository;
import it.chickenfries.lab.repository.RoleRepository;
import it.chickenfries.lab.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MessageService {

    private static final Logger LOG = LogManager.getLogger(MessageService.class.getCanonicalName());
    private MessageRepository messageRepository;
    private UserRepository userRepository;
    private ContextService contextService;
    private RoleRepository roleRepository;

    @Autowired
    public MessageService(
            ContextService contextService,
            UserRepository userRepository,
            MessageRepository messageRepository) {
        this.contextService = contextService;
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
    }

    /*
    * This method retrieves user inbox messages
    * @param Pageable (number of page, elements for page, sorted by message sentDateTime)
    * @return Page
    * */
    @Transactional
    public Page<Message> getMessages(Pageable pageable) {
        String email = contextService.getPrincipalMail();
        List<Sort.Order> orders = new ArrayList<>();
        Sort.Order oTime = pageable.getSort().getOrderFor("sentDateTime");
        Sort.Order oRead = pageable.getSort().getOrderFor("read");
        if (oTime!=null) {
            orders.add(oTime);
        }
        if(oRead != null){
            orders.add(oRead);
        }
        PageRequest pageRequest = PageRequest.of(
                pageable.getPageNumber(),
                pageable.getPageSize(),
                Sort.by(orders)
        );
        return this.messageRepository.findAllByUserEntity_Email(email, pageRequest);
    }

    @Transactional
    public void addRemoveShiftSendMessage(String line,
                                          LocalDate date,
                                          String email,
                                          DirectionType direction,
                                          Boolean add) {

        String templateMessage = add ?
                String.format("Nuovo turno il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line) :
                String.format("Rimosso turno il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);
        String subject = add ?
                String.format("Nuovo turno il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line) :
                String.format("Rimosso turno il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);
        Message message = Message.builder().
                id(UUID.randomUUID().toString()).
                message(templateMessage).
                subject(subject).
                userEntity(getUserWithException(email)).
                readDateTime(null).
                read(false).
                sentDateTime(LocalDateTime.now()).
                build();
        this.messageRepository.save(message);
    }

    @Transactional
    public void addRemoveAvailabilitySendMessage(String line, LocalDate day, String email, DirectionType direction, Boolean add) {

        List<UserEntity> administrators = null;

        try{
            administrators = contextService.getAdministrators(line);
        } catch (UserNotFoundException e){
            LOG.error(e.getMessage());
        }

        if (administrators != null) {

            UserEntity chaperon = contextService.getUserByEmail(email);

            String chaperonNameSurname = chaperon.getName()+" "+chaperon.getSurname();

            String templateMessage = add ?
                    String.format("Confermata disponibilità di %s il %s su linea %s", chaperonNameSurname, day.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line) :
                    String.format("Rimossa disponibilità di %s il %s su linea %s", chaperonNameSurname, day.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);

            String subject = add ?
                    String.format("Disponibilità di %s il %s", chaperonNameSurname, day.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"))) :
                    String.format("Disponibilità di %s il %s", chaperonNameSurname, day.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")));

            for (UserEntity user : administrators) {

                Message message = Message.builder().
                        id(UUID.randomUUID().toString()).
                        message(templateMessage).
                        subject(subject).
                        userEntity(user).
                        readDateTime(null).
                        read(false).
                        sentDateTime(LocalDateTime.now()).
                        build();
                this.messageRepository.save(message);

            }
        }
    }

    @Transactional
    public void addReservationSendMessage(String line, LocalDate date, ReservationRequestDto reservationRequestDto) {

        List<UserEntity> chaperons = null;

        try{
            chaperons = contextService.getChaperons(line, date, reservationRequestDto.getDirection());
        }catch (LineScheduleNotFoundException e){
            LOG.error(e.getMessage());
        }

        if(chaperons != null) {

            String templateMessage = String.format("Nuova prenotazione il %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")));
            String subject = String.format("Nuova prenotazione il %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")));

            for (UserEntity user : chaperons){
                Message message = Message.builder().
                        id(UUID.randomUUID().toString()).
                        message(templateMessage).
                        subject(subject).
                        userEntity(user).
                        readDateTime(null).
                        read(false).
                        sentDateTime(LocalDateTime.now()).
                        build();
                this.messageRepository.save(message);
            }
        }


    }

    @Transactional
    public void readMessage(String id) {
        Optional<Message> optionalMessage = this.messageRepository.findById(id);

        if (!optionalMessage.isPresent()) {
            throw new MessageNotFoundException();
        }

        Message message = optionalMessage.get();

        message.setRead(true);
        message.setReadDateTime(LocalDateTime.now());
        this.messageRepository.save(message);
    }

    private UserEntity getUserWithException(String email) {
        Optional<UserEntity> optUser = this.userRepository.findUserEntityByEmail(email);
        if(!optUser.isPresent()){
            throw new UserNotFoundException();
        }
        return optUser.get();
    }

/*    @Transactional
    public void updateReservationSendMessage(String line, LocalDate date, String ticket, ReservationUpdateRequestDto reservationUpdateRequestDto) {

        List<User> chaperons = null;

        try{
            chaperons = contextService.getChaperons(line, date, reservationUpdateRequestDto.getDirection());
        }catch (LineScheduleNotFoundException e){
            LOG.error(e.getMessage());
        }

        if(chaperons != null) {

            String templateMessage = String.format(" PRENOTAZIONE: ");
            String subject = String.format("AGGIORNATA PRENOTAZIONE");

            for (User user : chaperons){
                Message message = Message.builder().
                        id(UUID.randomUUID().toString()).
                        message(templateMessage).
                        subject(subject).
                        user(user).
                        readDateTime(null).
                        read(false).
                        sentDateTime(LocalDateTime.now()).
                        build();
                this.messageRepository.save(message);
            }
        }
    }*/

    @Transactional
    public void deleteReservationSendMessage(String ticket) {

        Reservation reservation;

        try{
            reservation = contextService.getReservationWithException(ticket);
        }catch (ReservationNotFoundException e){
            LOG.error(e.getMessage());
            return;
        }

        List<UserEntity> chaperons = null;
        String line = reservation.getLineSchedule().getLine().getLine();
        LocalDate date = reservation.getDate();
        DirectionType directionType = reservation.getDirection();


        try{
            chaperons = contextService.getChaperons(line, date, directionType);
        }catch (LineScheduleNotFoundException e){
            LOG.error(e.getMessage());
        }

        if(chaperons != null) {

            String templateMessage = String.format("Cancellata prenotazione il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);
            String subject = String.format("Cancellata prenotazione il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);

            for (UserEntity user : chaperons){
                Message message = Message.builder().
                        id(UUID.randomUUID().toString()).
                        message(templateMessage).
                        subject(subject).
                        userEntity(user).
                        readDateTime(null).
                        read(false).
                        sentDateTime(LocalDateTime.now()).
                        build();
                this.messageRepository.save(message);
            }
        }
    }

    public void collectChildSendMessage(String ticket, boolean collect) {
        Reservation reservation;
        String chaperon;
        try{
            reservation = contextService.getReservationWithException(ticket);
            UserEntity user = this.contextService.getContextUser();
            chaperon = user.getName() + " " + user.getSurname();
        }catch (ReservationNotFoundException | UserNotFoundException e){
            LOG.error(e.getMessage());
            return;
        }
        if (collect) {

            String place = reservation.getOn_B().getPlace();
            String child = reservation.getChild().getName();
            String subject = String.format("%s è in partenza" , child);
            String templateMessage = String.format("L'accompagnatore %s ha recuperato %s presso %s", chaperon, child, place);
            this.messageRepository.save(Message.builder().
                    id(UUID.randomUUID().toString()).
                    userEntity(reservation.getUserEntity()).
                    subject(subject).
                    message(templateMessage).
                    sentDateTime(LocalDateTime.now()).
                    readDateTime(null).
                    read(false).
                    build());

        }
    }

    @Transactional
    public void deliverChildSendMessage(String ticket, boolean deliver) {
        Reservation reservation;
        String chaperon;
        try{
            reservation = contextService.getReservationWithException(ticket);
            UserEntity user = this.contextService.getContextUser();
            chaperon = user.getName() + " " + user.getSurname();
        }catch (ReservationNotFoundException | UserNotFoundException e){
            LOG.error(e.getMessage());
            return;
        }
        if (deliver) {

            String place = reservation.getOn_B().getPlace();
            String child = reservation.getChild().getName();
            String subject = String.format("%s è arrivato" , child);
            String templateMessage = String.format("L'accompagnatore %s ha consegnato %s presso %s", chaperon, child, place);
            this.messageRepository.save(Message.builder().
                    id(UUID.randomUUID().toString()).
                    userEntity(reservation.getUserEntity()).
                    subject(subject).
                    message(templateMessage).
                    sentDateTime(LocalDateTime.now()).
                    readDateTime(null).
                    read(false).
                    build());

        }
    }

    public void addRemoveRoleSendMessage(boolean enable, String line, String email, UserRole userRole) {

        try{
            UserEntity user = this.contextService.getContextUser();
            String admin = user.getName() + " " + user.getSurname();

            String templateMessage = enable ?
                    String.format("Nuovo ruolo - %s su linea %s", userRole.toString(), line) :
                    String.format("Rimosso ruolo - %s su linea %s", userRole.toString(), line);
            String subject = enable ?
                    String.format("%s ti ha aggiunto come %s su linea %s", admin, userRole.toString(), line) :
                    String.format("%s ti ha rimosso come %s su linea %s", admin, userRole.toString(), line);
            Message message = Message.builder().
                    id(UUID.randomUUID().toString()).
                    message(templateMessage).
                    subject(subject).
                    userEntity(getUserWithException(email)).
                    readDateTime(null).
                    read(false).
                    sentDateTime(LocalDateTime.now()).
                    build();
            this.messageRepository.save(message);
        } catch (Exception e){
            LOG.error(e.getMessage());
        }

    }
}
