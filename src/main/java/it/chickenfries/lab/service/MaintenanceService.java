package it.chickenfries.lab.service;

import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.persistency.entity.*;
import it.chickenfries.lab.repository.LineRepository;
import it.chickenfries.lab.repository.UserRepository;
import it.chickenfries.lab.setup.SetupLineDto;
import it.chickenfries.lab.setup.SetupStopLineDto;
import it.chickenfries.lab.setup.SetupUserDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MaintenanceService {

    private final LineRepository lineRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    @Value("${setup-password}") private String password;
    
    public MaintenanceService(LineRepository lineRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.lineRepository = lineRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /*
    * if line already exists add - if new - the administrator, otherwise create also line*/
    @Transactional
    void addAdministrator(SetupLineDto setupLineDto){

        Optional<Line> optLine = lineRepository.findByLine(setupLineDto.getLine());
        Optional<UserEntity> optUser = userRepository.findUserEntityByEmail(setupLineDto.getAdmin().getEmail());

        Line line = optLine.orElseGet(() -> buildLine(setupLineDto.getPlaces(), setupLineDto.getLine()));
        UserEntity user = optUser.orElseGet(() -> buildLineAdministrator(setupLineDto.getAdmin(), line));

        lineRepository.save(line);
        userRepository.save(user);
    }

    private Line buildLine(List<SetupStopLineDto> places, String _line){
        Line line = new Line();
        List<LineStop> stops = buildLineStopList(line, places);
        List<LineSchedule> schedules = buildSchedule(line);
        line.setLine(_line);
        line.setStops(stops);
        line.setLineSchedules(schedules);
        return line;
    }

    private UserEntity buildLineAdministrator(SetupUserDto userDto, Line line){
        Role role = new Role();
        HashSet<Role> roles =new HashSet<>();
        roles.add(role);

        UserEntity user = UserEntity.builder()
                .email(userDto.getEmail())
                .surname(userDto.getSurname())
                .name(userDto.getName())
                .password(passwordEncoder.encode(password))
                .enabled(true)
                .roles(new HashSet<>(roles))
                .build();

        role.setUserEntity(user);
        role.setLine(line);
        role.setUserRole(UserRole.LINE_ADMINISTRATOR);

        return user;
    }

    private List<LineStop> buildLineStopList(Line line, List<SetupStopLineDto> places) {
        return places.stream().map(sl-> LineStop.builder().
                place(sl.getPlace()).
                line(line).
                toSchool(sl.getToSchool()).
                toHome(sl.getToHome()).
                lat(sl.getLat()).
                lon(sl.getLon()).
                build())
                .collect(Collectors.toList());
    }

    private List<LineSchedule> buildSchedule(Line line){
        List<LineSchedule> lineYearSchedule = new ArrayList<>();
        for(int i =0; i < 10; i++ ){
            LineSchedule lineSchedule = new LineSchedule();
            lineSchedule.setLine(line);
            lineSchedule.setDay(LocalDate.now().plusDays(i));
            lineYearSchedule.add(lineSchedule);
        }
        return lineYearSchedule;
    }

}
