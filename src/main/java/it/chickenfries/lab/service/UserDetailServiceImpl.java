package it.chickenfries.lab.service;

import it.chickenfries.lab.model.RoleAuthority;
import it.chickenfries.lab.persistency.entity.UserEntity;
import it.chickenfries.lab.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.stream.Collectors;

@Primary
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserDetailServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Optional<UserEntity> optUser = userRepository.findUserEntityByEmail(email);
        if (!optUser.isPresent()) {
            throw new UsernameNotFoundException("User not found");
        }
        UserEntity user = optUser.get();
        return new org.springframework.security.core.userdetails.User(
                user.getEmail(),
                user.getPassword(),
                user.getRoles().stream().map(role -> new RoleAuthority(role.getUserRole(), role.getLine().getLine())).collect(Collectors.toSet()));
    }
}
