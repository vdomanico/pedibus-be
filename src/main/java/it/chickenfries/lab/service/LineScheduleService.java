package it.chickenfries.lab.service;

import it.chickenfries.lab.model.exception.LineScheduleNotFoundException;
import it.chickenfries.lab.persistency.entity.LineSchedule;
import it.chickenfries.lab.repository.LineScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class LineScheduleService {

    private final LineScheduleRepository lineScheduleRepository;

    @Autowired
    public LineScheduleService(LineScheduleRepository lineScheduleRepository) {
        this.lineScheduleRepository = lineScheduleRepository;
    }

    LineSchedule getLineScheduleWithException(LocalDate date, String line){
        Optional<LineSchedule> optSchedule = lineScheduleRepository.findByDayAndLine_Line(date, line);
        if(!optSchedule.isPresent()) {
            throw new LineScheduleNotFoundException();
        }
        return optSchedule.get();
    }

}
