package it.chickenfries.lab.service;

import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.model.exception.LineNotFoundException;
import it.chickenfries.lab.model.exception.LineScheduleNotFoundException;
import it.chickenfries.lab.model.exception.ReservationNotFoundException;
import it.chickenfries.lab.model.exception.UserNotFoundException;
import it.chickenfries.lab.persistency.entity.*;
import it.chickenfries.lab.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ContextService {

    private final UserRepository userRepository;
    private final LineScheduleRepository lineScheduleRepository;
    private final LineRepository lineRepository;
    private final ReservationRepository reservationRepository;
    private final RoleRepository roleRepository;
    private final MessageRepository messageRepository;

    @Autowired
    public ContextService(UserRepository userRepository,
                          LineScheduleRepository lineScheduleRepository,
                          LineRepository lineRepository,
                          ReservationRepository reservationRepository,
                          RoleRepository roleRepository,
                          MessageRepository messageRepository
                          ){
        this.userRepository = userRepository;
        this.lineScheduleRepository = lineScheduleRepository;
        this.lineRepository = lineRepository;
        this.reservationRepository = reservationRepository;
        this.roleRepository = roleRepository;
        this.messageRepository = messageRepository;
    }

    public String getPrincipalMail(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (String) auth.getPrincipal();
    }

    @Transactional
    public UserEntity getContextUserWithException(){

        String userEmail = getPrincipalMail();
        Optional<UserEntity> optionalUser = userRepository.findUserEntityByEmail(userEmail);

        if(!optionalUser.isPresent()){
            throw new UserNotFoundException();
        }

        return optionalUser.get();
    }

    @Transactional
    public UserEntity getUserByEmail(String email) {
        Optional<UserEntity> optionalUser = userRepository.findUserEntityByEmail(email);

        if (!optionalUser.isPresent())
            throw new UserNotFoundException();

        return optionalUser.get();
    }

    @Transactional
    public UserEntity getContextUser(){

        String userEmail = getPrincipalMail();
        Optional<UserEntity> optionalUser = userRepository.findUserEntityByEmail(userEmail);

        return optionalUser.orElse(null);

    }

    @Transactional
    public List<UserEntity> getChaperons(String line, LocalDate date, DirectionType direction) {
        Optional<LineSchedule> optionalLineSchedule = this.lineScheduleRepository.findByDayAndLine_Line(date, line);

        if(!optionalLineSchedule.isPresent()){
            throw new LineScheduleNotFoundException();
        }
        LineSchedule lineSchedule = optionalLineSchedule.get();
        return lineSchedule.getShifts().stream().
                filter(shift -> shift.getDirection().toString().equals(direction.toString())).
                map(Shift::getUserEntity).collect(Collectors.toList());
    }

    @Transactional
    List<UserEntity> getAdministrators(String line) {

        Optional<Line> optionalLine = lineRepository.findByLine(line);

        if (!optionalLine.isPresent())
            throw new LineNotFoundException();

        return roleRepository.findAllByLine_line(line).stream()
                .filter(role -> role.getUserRole().equals(UserRole.LINE_ADMINISTRATOR))
                .map(Role::getUserEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Reservation getReservationWithException(String ticket) {
        Optional<Reservation> optionalReservation = this.reservationRepository.findByTicket(ticket);

        if (!optionalReservation.isPresent()) {
            throw new ReservationNotFoundException();
        }
        return optionalReservation.get();
    }

    @Transactional
    public int getUnreadMessages(String email) {
        return this.messageRepository.countAllByUserEntity_EmailAndRead(email, false);
    }
}
