package it.chickenfries.lab.service;

import it.chickenfries.lab.dto.line.LineResponseDto;
import it.chickenfries.lab.dto.line.LineStopResponseDto;
import it.chickenfries.lab.dto.line.LinesResponseDto;
import it.chickenfries.lab.model.exception.LineNotFoundException;
import it.chickenfries.lab.persistency.entity.Line;
import it.chickenfries.lab.persistency.entity.LineStop;
import it.chickenfries.lab.repository.LineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LineService {

    private final LineRepository lineRepository;
    private final  MailService mailService;

    @Autowired
    public LineService(LineRepository lineRepository, MailService mailService) {
        this.lineRepository = lineRepository;
        this.mailService = mailService;
    }

    public LinesResponseDto getLines(){
        LinesResponseDto linesResponseDto = new LinesResponseDto();
        List<String> names = new ArrayList<>();
        Iterable<Line> res = lineRepository.findAll();
        res.forEach(line -> names.add(line.getLine()));
        linesResponseDto.setLines(names);
        return linesResponseDto;

    }

    @Transactional
    public LineResponseDto getLine(String lineId){

        Optional<Line> optLine = lineRepository.findOneByLine(lineId);

        if(!optLine.isPresent()){
            throw new LineNotFoundException();
        }

        Line line = optLine.get();
        String name = line.getLine();
        List<LineStop> stops = line.getStops();
        List<LineStopResponseDto> toSchool = stops.stream().map(s -> new LineStopResponseDto(s.getPlace(), s.getToSchool())).collect(Collectors.toList());
        List<LineStopResponseDto> toHome = stops.stream().map(s -> new LineStopResponseDto(s.getPlace(),s.getToHome())).collect(Collectors.toList());
        toSchool.sort(Comparator.comparing(LineStopResponseDto::getTime));
        toHome.sort(Comparator.comparing(LineStopResponseDto::getTime));

        LineResponseDto lineResponseDto = new LineResponseDto();
        lineResponseDto.setLine(name);
        lineResponseDto.setToSchool(toSchool);
        lineResponseDto.setToHome(toHome);

        return lineResponseDto;
    }

    //TODO: when new line is registered and added to DB send email to user
    public void registerLine(Line line) { }
}
