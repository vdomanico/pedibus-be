package it.chickenfries.lab.service;

import com.google.firebase.messaging.FirebaseMessagingException;
import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.persistency.entity.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Set;

@Service
public class PushNotificationService {

    private final Logger logger = LoggerFactory.getLogger(PushNotificationService.class);
    private final FirebaseCloudMessagingService firebaseCloudMessagingService;

    @Autowired
    public PushNotificationService(FirebaseCloudMessagingService firebaseCloudMessagingService, ContextService contextService) {
        this.firebaseCloudMessagingService = firebaseCloudMessagingService;
    }


    @Async("asyncThreadTaskExecutor")
    public void addRemoveShiftSendNotification(String line,
                                               LocalDate date,
                                               UserEntity user,
                                               String email,
                                               DirectionType direction,
                                               Boolean add){

        if(user == null){
            logger.error("user with email " + email + "not found - cannot send pushNotification");
            return;
        }

        Set<String> registrationTokens = user.getRegistrationsToken();

        if(registrationTokens == null || registrationTokens.isEmpty()){
            return;
        }

        String templateMessage = add ?
                String.format("Nuovo turno il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line) :
                String.format("Rimosso turno il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);
        String subject = add ?
                String.format("Nuovo turno il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line) :
                String.format("Rimosso turno il %s - linea %s", date.format(DateTimeFormatter.ofPattern("dd-MM-YYYY")), line);

        try {
            this.firebaseCloudMessagingService.sendNotification(new ArrayList<>(registrationTokens), subject, templateMessage);
        } catch (FirebaseMessagingException e) {
            logger.error(e.getMessage());
        }

    }

    @Async("asyncThreadTaskExecutor")
    public void addRemoveRoleSendNotification(boolean enable, String line,
                                              UserEntity user,
                                              String email, UserRole userRole) {


        if(user == null){
            logger.error("user with email " + email + "not found - cannot send pushNotification");
            return;
        }


        Set<String> registrationTokens = user.getRegistrationsToken();

        if(registrationTokens == null || registrationTokens.isEmpty()){
            return;
        }

        String subject =enable  ? "Nuovo ruolo" : "Rimosso ruolo";
        String templateMessage = enable ?
                String.format("Nuovo ruolo %s linea %s", userRole.toString(), line) :
                String.format("Rimosso ruolo %s linea %s", userRole.toString(), line);

        try {
            this.firebaseCloudMessagingService.sendNotification(new ArrayList<>(registrationTokens), subject, templateMessage);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }
}
