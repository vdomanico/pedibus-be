package it.chickenfries.lab.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class MailService {

    private static final Logger LOG = LogManager.getLogger(MailService.class.getCanonicalName());
    private final JavaMailSender javaMailSender;
    @Value("${spring.mail.username}") private String emailFrom;
    @Value("${enable-email:false}") private boolean enabled;

    @Autowired
    public MailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMail(String email, String subject, String message) throws MailException {
        LOG.info("Sending mail to " + email + " - [enabled: " + enabled + "]");

        if (enabled) {
            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(email);
            mail.setFrom(emailFrom);
            mail.setSubject(subject);
            mail.setText(message);
            javaMailSender.send(mail);

        }

    }

    public void collectChildSendMail(String ticket, boolean collect){
        // TODO
    }

    public void deliverChildSendMail(String ticket, boolean deliver) {
        // TODO
    }
}
