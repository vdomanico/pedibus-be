package it.chickenfries.lab.service;

import it.chickenfries.lab.dto.children.ChildReservationResponseDto;
import it.chickenfries.lab.dto.children.ChildReservationStatusResponseDto;
import it.chickenfries.lab.dto.children.ChildResponseDto;
import it.chickenfries.lab.dto.children.ReservationChildRequestDto;
import it.chickenfries.lab.dto.reservation.*;
import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.model.exception.*;
import it.chickenfries.lab.persistency.entity.*;
import it.chickenfries.lab.repository.ReservationRepository;
import it.chickenfries.lab.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;
    private final LineScheduleService lineScheduleService;
    private final UserRepository userRepository;
    private final ChildService childService;
    private final ContextService contextService;

    @Autowired
    public ReservationService(ContextService contextService,
            ReservationRepository reservationRepository,
                              LineScheduleService lineScheduleService,
                              UserRepository userRepository,
                              ChildService childService) {
        this.contextService = contextService;
        this.lineScheduleService = lineScheduleService;
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.childService = childService;
    }

    public ReservationDetailResponseDto getReservation(String line, LocalDate date, String ticket) {
        Reservation reservation = getReservationWithException(ticket);
        DirectionType directionType = reservation.getDirection();
        Child child = reservation.getChild();
        return ReservationDetailResponseDto.builder().
                ticketId(reservation.getTicket()).
                child(ChildResponseDto.builder().email(child.getUserEntity().getEmail()).name(child.getName()).surname(child.getSurname()).build()).
                timeOffBoard(directionType==DirectionType.TO_HOME?reservation.getOff_B().getToHome():reservation.getOff_B().getToSchool()).
                offBoard(reservation.getOff_B().getPlace()).
                timeOnBoard(directionType==DirectionType.TO_HOME?reservation.getOn_B().getToHome():reservation.getOn_B().getToSchool()).
                onBoard(reservation.getOn_B().getPlace()).
                day(date).
                build();
    }

    @Transactional
    public LineReservationResponseDto getLineReservations(String _line, LocalDate date) {

        LineReservationResponseDto lineReservationResponseDto = new LineReservationResponseDto();
        LineSchedule lineSchedule = lineScheduleService.getLineScheduleWithException(date, _line);
        Line line = lineSchedule.getLine();

        List<LineStopReservationResponseDto> toHome = new ArrayList<>();
        List<LineStopReservationResponseDto> toSchool = new ArrayList<>();
        List<LineStop> lineStops = line.getStops();

        for(LineStop ls : lineStops){
            List<Reservation> reservationOnB = ls.getOn_board().
                    stream().
                    filter(reservation -> reservation.getLineSchedule().getDay().toEpochDay() == date.toEpochDay() && !reservation.isDeleted()).
                    collect(Collectors.toList());
            List<Reservation> reservationOffB = ls.getOff_board().
                    stream().
                    filter(reservation -> reservation.getLineSchedule().getDay().toEpochDay() == date.toEpochDay() && !reservation.isDeleted()).
                    collect(Collectors.toList());
            toHome.add(getLineStopReservationDto(ls,reservationOnB,reservationOffB,DirectionType.TO_HOME));
            toSchool.add(getLineStopReservationDto(ls,reservationOnB,reservationOffB,DirectionType.TO_SCHOOL));
        }
        toSchool.sort(Comparator.comparing(LineStopReservationResponseDto::getTime));
        toHome.sort(Comparator.comparing(LineStopReservationResponseDto::getTime));
        lineReservationResponseDto.setLine(_line);
        lineReservationResponseDto.setDay(date);
        lineReservationResponseDto.setToHome(toHome);
        lineReservationResponseDto.setToSchool(toSchool);

        return lineReservationResponseDto;
    }

    @Transactional
    public LineReservationResponseDto getParentLineDayScheduleReservation(
            String _line,
            LocalDate date) {

        UserEntity parent = contextService.getContextUserWithException();
        LineSchedule lineSchedule = lineScheduleService.getLineScheduleWithException(date, _line);
        Line line = lineSchedule.getLine();

        List<LineStopReservationResponseDto> toHome = new ArrayList<>();
        List<LineStopReservationResponseDto> toSchool = new ArrayList<>();
        List<LineStop> lineStops = line.getStops();

        List<Reservation> reservationsToSchool = lineSchedule.getReservations().stream().filter(reservation -> reservation.getUserEntity().getEmail().equals(parent.getEmail())
                && reservation.getDirection().toString().equals(DirectionType.TO_SCHOOL.toString())
                && !reservation.isDeleted()).collect(Collectors.toList());

        List<Reservation> reservationsToHome = lineSchedule.getReservations().stream().filter(reservation -> reservation.getUserEntity().getEmail().equals(parent.getEmail())
                && reservation.getDirection().toString().equals(DirectionType.TO_HOME.toString())
                && !reservation.isDeleted()).collect(Collectors.toList());

        for(LineStop ls : lineStops){
            toHome.add(getLineStopReservationDto(ls, reservationsToHome, DirectionType.TO_HOME));
            toSchool.add(getLineStopReservationDto(ls, reservationsToSchool, DirectionType.TO_SCHOOL));
        }

        toSchool.sort(Comparator.comparing(LineStopReservationResponseDto::getTime));
        toHome.sort(Comparator.comparing(LineStopReservationResponseDto::getTime));

        return LineReservationResponseDto.
                builder().
                line(_line).
                day(date).
                toHome(toHome).
                toSchool(toSchool).
                children(getParentChildrenInfoReservationInDate(parent, date)).
                build();
    }

    @Transactional
    public List<ChildReservationStatusResponseDto> getParentChildrenInfoReservationInDate(UserEntity parent, LocalDate date) {
        List<ChildReservationStatusResponseDto> childrenDto = new ArrayList<>();
        for(Child c : parent.getChildren()){
            List<Reservation> rs = this.reservationRepository.findByDateAndChild(date, c);
            Reservation reservationToHome = rs.stream().
                    filter(rr -> rr.getDirection().toString().equals(DirectionType.TO_HOME.toString())  && !rr.isDeleted()).
                    findAny().orElse(null);
            Reservation reservationToSchool = rs.stream().
                    filter(rr -> rr.getDirection().toString().equals(DirectionType.TO_SCHOOL.toString()) && !rr.isDeleted()).
                    findAny().orElse(null);
            boolean reservedToHome = reservationToHome != null;
            boolean reservedToSchool = reservationToSchool != null;
            childrenDto.add(ChildReservationStatusResponseDto.builder().
                    name(c.getName()).
                    surname(c.getSurname()).
                    email(c.getUserEntity().getEmail()).
                    date(date).
                    reservedToHome(reservedToHome).
                    reservedToSchool(reservedToSchool).
                    lineToHome(reservedToHome ? reservationToHome.getLineSchedule().getLine().getLine() : null).
                    ticketToHome(reservedToHome ? reservationToHome.getTicket() : null).
                    lineToSchool(reservedToSchool ? reservationToSchool.getLineSchedule().getLine().getLine() : null).
                    ticketToSchool(reservedToSchool ? reservationToSchool.getTicket() : null).
                    build());
        }
        return childrenDto;
    }

    /*
    * The administrator of the line, so not necessary the parent
    * can make a new reservation
    * */
    @Transactional
    public Map<String, ChildResponseDto> addReservation(String line, LocalDate date, ReservationRequestDto reservationRequestDto) {

        List<ReservationChildRequestDto> children = reservationRequestDto.getChildren();
        UserEntity applier = contextService.getContextUserWithException();
        boolean toSchool = reservationRequestDto.getDirection().toString().equals(DirectionType.TO_SCHOOL.toString());
        LineSchedule lineSchedule = lineScheduleService.getLineScheduleWithException(date,line);
        List<LineStop> lineStops = lineSchedule.getLine().getStops();
        List<Reservation> _reservations = reservationRepository.findByDateAndDirection(date, reservationRequestDto.getDirection());

        if(_reservations.
                stream().
                anyMatch(reservation -> !reservation.isDeleted() &&
                        reservationRequestDto.getChildren().stream().
                                anyMatch(
                                        c -> c.getName().equalsIgnoreCase(reservation.getChild().getName())
                                                &&
                                                c.getSurname().equalsIgnoreCase(reservation.getChild().getSurname())))
        ){
            throw new ReservationConflictException();
        }

        String selectedPlace = reservationRequestDto.getPlace();
        LineStop schoolPlace = getSchoolPlace(lineStops, reservationRequestDto.getDirection());

        checkLineStopsBelongSameLineWithException(lineStops, selectedPlace, schoolPlace.getPlace());

        Map<String,LineStop> map = lineStops.stream().collect(Collectors.toMap(LineStop::getPlace, Function.identity()));
        LineStop onOffBoard = map.get(selectedPlace);

        if (date.isEqual(LocalDate.now())) {
            Time time = toSchool ? onOffBoard.getToSchool() : onOffBoard.getToHome();
            if (time.toLocalTime().isBefore(LocalTime.now()))
                throw new ReservationNotAddedException();
        }

        List<Reservation> reservations = applier.getReservations();
        if(reservations == null){
            reservations = new ArrayList<>();
        }

        Map<String, ChildResponseDto> tickets = new HashMap<>();
        for(ReservationChildRequestDto reservationChildRequestDto : children){
            Child child = childService.getChildWithException(
                    reservationChildRequestDto.getEmail(), reservationChildRequestDto.getName(), reservationChildRequestDto.getSurname());
            UUID random = UUID.randomUUID();
            Reservation reservation = buildReservation(
                    random.toString(),
                    date,
                    lineSchedule,
                    child,
                    applier,
                    toSchool ? onOffBoard : schoolPlace,
                    toSchool ? schoolPlace : onOffBoard,
                    reservationRequestDto.getDirection());
            reservations.add(reservation);
            tickets.put(random.toString(),
                    ChildResponseDto.builder().name(child.getName()).surname(child.getSurname()).email(child.getUserEntity().getEmail()).build());
        }

        userRepository.save(applier);
        return tickets;
    }

    private LineStop getSchoolPlace(List<LineStop> lineStops, DirectionType direction) {
        lineStops.sort((o1, o2) ->
                direction.toString().
                        equals(DirectionType.TO_SCHOOL.toString()) ?
                        LocalTime.parse(o1.getToSchool().toString()).compareTo(LocalTime.parse(o2.getToSchool().toString())) :
                        LocalTime.parse(o1.getToSchool().toString()).compareTo(LocalTime.parse(o2.getToSchool().toString())));
        return lineStops.get(lineStops.size()-1);
    }

    public void updateReservation(String line, LocalDate date, String ticket, ReservationUpdateRequestDto reservationUpdateRequestDto) {

        Reservation reservation = getReservationWithException(ticket);
        UserEntity parent = reservation.getUserEntity();

        ReservationChildRequestDto childDto = reservationUpdateRequestDto.getChild();
        String selectedPlace = reservationUpdateRequestDto.getPlace();
        DirectionType direction = reservationUpdateRequestDto.getDirection();
        LineSchedule lineSchedule = lineScheduleService.getLineScheduleWithException(date,line);
        List<LineStop> lineStops = lineSchedule.getLine().getStops();

        // If scheduled for now and if Pedibus is already arrived to the stop
        // not possible to update reservation
        if (LocalDate.now().isEqual(lineSchedule.getDay())) {
            if (reservation.getOn_B().getToSchool().toLocalTime().isBefore(LocalTime.now())) {
                throw new ReservationNotUpdatableException();
            }
            else if (reservation.getOff_B().getToSchool().toLocalTime().isBefore(LocalTime.now())) {
                throw new ReservationNotUpdatableException();
            }
        }

        LineStop schoolPlace = getSchoolPlace(lineStops, reservationUpdateRequestDto.getDirection());
        checkLineStopsBelongSameLineWithException(lineStops, selectedPlace, schoolPlace.getPlace());
        Map<String,LineStop> map = lineStops.stream().collect(Collectors.toMap(LineStop::getPlace, Function.identity()));
        LineStop onOffBoard = map.get(selectedPlace);


        Child child = childService.getChildWithException(childDto.getEmail(), childDto.getName(), childDto.getSurname());
        reservation.setChild(child);
        reservationUpdateRequestDto.setDirection(reservationUpdateRequestDto.getDirection());
        reservation.setOn_B(direction.toString().equals(DirectionType.TO_SCHOOL.toString()) ? onOffBoard : schoolPlace);
        reservation.setOff_B(direction.toString().equals(DirectionType.TO_HOME.toString()) ? onOffBoard : schoolPlace);
        userRepository.save(parent);

    }

    /*
    * I want to record reservations deleted (delete is logical)
    * */
    @Transactional
    public void deleteReservation(String ticket) {
        Reservation reservation = getReservationWithException(ticket);

        LineStop offB = reservation.getOff_B();
        LineStop onB = reservation.getOn_B();
        LineSchedule lineSchedule = reservation.getLineSchedule();

        // If scheduled for now and if Pedibus is already arrived to the stop
        // not possible to delete reservation
        if (LocalDate.now().isEqual(lineSchedule.getDay())) {
            if (onB.getToSchool().toLocalTime().isBefore(LocalTime.now())) {
                throw new ReservationNotEraseableException();
            }
            else if (offB.getToSchool().toLocalTime().isBefore(LocalTime.now())) {
                throw new ReservationNotEraseableException();
            }
        }

        reservation.setDeleted(true);
        userRepository.save(reservation.getUserEntity());
    }

    @Transactional
    public void collectChild(String ticket, boolean collect) {

        Reservation res = getReservationWithException(ticket);
        UserEntity user = res.getChild().getUserEntity();
        Reservation reservation = user.getReservations().stream().filter(r -> r.getTicket().equals(ticket)).findFirst().orElse(null);

        if(reservation == null){
            throw new ReservationNotFoundException();
        }
        reservation.setCollected(collect);

        userRepository.save(user);
    }

    @Transactional
    public void deliverChild(String ticket, boolean deliver) {

        Reservation res = getReservationWithException(ticket);
        UserEntity user = res.getChild().getUserEntity();
        Reservation reservation = user.getReservations().stream().filter(r -> r.getTicket().equals(ticket)).findFirst().orElse(null);

        if(reservation == null){
            throw new ReservationNotFoundException();
        }
        reservation.setDelivered(deliver);

        userRepository.save(user);
    }

    private LineStopReservationResponseDto getLineStopReservationDto(LineStop lineStop,
                                                                     List<Reservation> reservationOnB,
                                                                     List<Reservation> reservationOffB,
                                                                     DirectionType direction) {
        LineStopReservationResponseDto lineStopReservationResponseDto = new LineStopReservationResponseDto();
        lineStopReservationResponseDto.setPlace(lineStop.getPlace());
        lineStopReservationResponseDto.setTime(direction==DirectionType.TO_HOME?lineStop.getToHome():lineStop.getToSchool());

        List<ChildReservationResponseDto> walkersOnB = reservationOnB.stream().
                filter(reservation -> reservation.getDirection()==direction).
                map( r -> {
                    Child child = r.getChild();
                    return ChildReservationResponseDto.builder().
                            name(child.getName()).
                            surname(child.getSurname()).
                            email(child.getUserEntity().getEmail()).
                            ticket(r.getTicket()).
                            delivered(r.isDelivered()).
                            collected(r.isCollected()).
                            build();
                        }
                 ).collect(Collectors.toList());

        List<ChildReservationResponseDto> walkersOffB = reservationOffB.stream().
                filter(reservation -> reservation.getDirection()==direction).
                map( r -> {
                            Child child = r.getChild();
                            return ChildReservationResponseDto.builder().
                                    name(child.getName()).
                                    surname(child.getSurname()).
                                    email(child.getUserEntity().getEmail()).
                                    ticket(r.getTicket()).
                                    delivered(r.isDelivered()).
                                    collected(r.isCollected()).
                                    build();
                        }
                ).collect(Collectors.toList());

        lineStopReservationResponseDto.setOnBoard(walkersOnB);
        lineStopReservationResponseDto.setOffBoard(walkersOffB);
        return lineStopReservationResponseDto;
    }

    private LineStopReservationResponseDto getLineStopReservationDto(LineStop ls,
                                                                     List<Reservation> reservations,
                                                                     DirectionType direction) {
        List<ChildReservationResponseDto> onB = new ArrayList<>();
        List<ChildReservationResponseDto> offB = new ArrayList<>();

                reservations.forEach(reservation -> {
                    if(reservation.getDirection().toString().equals(direction.toString())){
                        Child child = reservation.getChild();
                        LineStop on = reservation.getOn_B();
                        LineStop off = reservation.getOff_B();
                        if(on.getPlace().equals(ls.getPlace())){
                            onB.add(ChildReservationResponseDto.builder().
                                    name(child.getName()).
                                    surname(child.getSurname()).
                                    email(child.getUserEntity().getEmail()).
                                    ticket(reservation.getTicket()).
                                    build());
                        }
                        if(off.getPlace().equals(ls.getPlace())){
                            offB.add(ChildReservationResponseDto.builder().
                                    name(child.getName()).
                                    surname(child.getSurname()).
                                    email(child.getUserEntity().getEmail()).
                                    ticket(reservation.getTicket()).
                                    build());
                        }
                    }
                });
                return LineStopReservationResponseDto.builder().
                        place(ls.getPlace()).
                        time(direction.toString().equals(DirectionType.TO_SCHOOL.toString()) ? ls.getToSchool() : ls.getToHome()).
                        onBoard(onB).
                        offBoard(offB).
                        latitude(ls.getLat()).
                        longitude(ls.getLon()).
                        build();
    }

    private void checkLineStopsBelongSameLineWithException(
            List<LineStop> lineStops,
            String placeOffBoard,
            String placeOnBoard) {
        List<String> places = lineStops.stream().map(LineStop::getPlace).collect(Collectors.toList());
        if(!places.containsAll(Arrays.asList(placeOffBoard,placeOnBoard))){
            throw new LineStopNotFoundException();
        }
    }

    private Reservation buildReservation(
            String ticket,
            LocalDate date,
            LineSchedule lineSchedule,
            Child child,
            UserEntity applier,
            LineStop onBoard,
            LineStop offBoard,
            DirectionType directionType){
        return Reservation.builder().
                deleted(false).
                ticket(ticket).
                date(date).
                lineSchedule(lineSchedule).
                child(child).
                userEntity(applier).
                on_B(onBoard).
                off_B(offBoard).
                direction(directionType).
                build();

    }

    private Reservation getReservationWithException(String ticket){
        Optional<Reservation> optRes = reservationRepository.findByTicket(ticket);

        if(!optRes.isPresent()){
            throw new ReservationNotFoundException();
        }

        Reservation reservation = optRes.get();

        if (reservation.isDeleted()){
            throw new ReservationNotFoundException();
        }

        return reservation;
    }

}