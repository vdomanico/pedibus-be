package it.chickenfries.lab.service;

import it.chickenfries.lab.dto.children.ChildResponseDto;
import it.chickenfries.lab.dto.user.UserAuthResponseDto;
import it.chickenfries.lab.dto.user.UserResponseDto;
import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.model.exception.*;
import it.chickenfries.lab.persistency.entity.Child;
import it.chickenfries.lab.persistency.entity.Line;
import it.chickenfries.lab.persistency.entity.Role;
import it.chickenfries.lab.persistency.entity.UserEntity;
import it.chickenfries.lab.repository.LineRepository;
import it.chickenfries.lab.repository.RoleRepository;
import it.chickenfries.lab.repository.UserRepository;
import it.chickenfries.lab.security.JwtTokenProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserService{

    private static final Logger LOG = LogManager.getLogger(UserService.class.getCanonicalName());
    private final ContextService contextService;
    private final UserRepository userRepository;
    private final LineRepository lineRepository;
    private final RoleRepository roleRepository;
    private final  MailService mailService;
    private PasswordEncoder passwordEncoder;
    private JwtTokenProvider jwtTokenProvider;
    @Value("${fe-url}") private String confirmationUrl;

    @Autowired
    public UserService(
                UserRepository userRepository,
                       RoleRepository roleRepository,
                       JwtTokenProvider jwtTokenProvider,
                       LineRepository lineRepository,
                       PasswordEncoder bCryptPasswordEncoder,
                        ContextService contextService,
                       MailService mailService) {
        this.contextService = contextService;
        this.userRepository = userRepository;
        this.lineRepository = lineRepository;
        this.roleRepository = roleRepository;
        this.jwtTokenProvider = jwtTokenProvider;
        this.mailService = mailService;
        this.passwordEncoder = bCryptPasswordEncoder;
    }

    public UserAuthResponseDto login(String email, String password) {
        Optional<UserEntity> optUser = userRepository.findUserEntityByEmailAndEnabled(email,true);
        if(!optUser.isPresent())
            throw new AuthenticationFailedException("User not found or account not enabled");
        UserEntity user = optUser.get();
        if(!passwordEncoder.matches(password,user.getPassword())){
            throw new AuthenticationFailedException();
        }

        return UserAuthResponseDto.builder().
                token(jwtTokenProvider.createToken(email, user.getUuid())).
                name(user.getName()).
                surname(user.getSurname()).
                email(email).
                roles(getUserLineGrants(user)).
                children(user.getChildren().stream().map(
                        c -> new ChildResponseDto(c.getName(),c.getSurname(),c.getUserEntity().getEmail()))
                        .collect(Collectors.toList())).
                unreadMessages(this.contextService.getUnreadMessages(email)).
                build();
    }


    public UserAuthResponseDto getUserInfo() {

        UserEntity user = this.contextService.getContextUserWithException();

        return UserAuthResponseDto.builder().
                name(user.getName()).
                surname(user.getSurname()).
                email(user.getEmail()).
                roles(getUserLineGrants(user)).
                children(user.getChildren().stream().map(
                        c -> new ChildResponseDto(c.getName(),c.getSurname(),c.getUserEntity().getEmail()))
                        .collect(Collectors.toList())).
                build();
    }

    /*
    * If multiples calls - only the last one is valid
    * The administrator of the line "invites" a new user
    */
    public void register(
            String email,
            String password,
            String name,
            String surname,
            Set<UserRole> userRoles,
            String line){

        Optional<UserEntity> optUser = userRepository.findUserEntityByEmailAndEnabled(email,true);
        Optional<Line> targetLine = lineRepository.findOneByLine(line);

        if(optUser.isPresent()) {
            throw new UserConflictException("E-mail already in use");
        }

        if(!targetLine.isPresent()){
            throw new LineNotFoundException();
        }

        String rUUID = UUID.randomUUID().toString();
        Set<Role> roles = new HashSet<>();

        long expireRegistrationTime = LocalDateTime.now().plusDays(1).atZone(ZoneOffset.UTC).toEpochSecond(); //un giorno di tempo di validità
        UserEntity user = UserEntity.builder()
                .email(email)
                .name(name)
                .surname(surname)
                .password(passwordEncoder.encode(password))
                .enabled(false)
                .uuid(rUUID)
                .registrationTime(expireRegistrationTime)
                .roles(roles)
                .build();

        for(UserRole ur : userRoles){
            Role role = new Role();
            roles.add(role);
            role.setUserEntity(user);
            role.setUserRole(ur);
            role.setLine(targetLine.get());
        }

        userRepository.save(user);

        String confirmationLink = confirmationUrl + "/confirm/" + rUUID;
        String template = String.format("Ciao %s,\n" +
                "conferma il tuo account pedibus!\n\n" +
                "%s\n\n" +
                "La tua email per accedere è %s \n" +
                "La tua password per accedere è %s \n\n" +
                "Hai tempo 24 ore dopo le quali dovrai richiedere all'amministratore una nuova registrazione.\n\n" +
                "Cordialmente,\n" +
                "Il Team Pedibus",
                name,confirmationLink,email,password);

            mailService.sendMail(email,
                    "Attivazione Account Pedibus",
                    template);

        LOG.info("register: confirmationLink:" + confirmationLink
                + " expires at " + expireRegistrationTime
                + " email: " + email
                + " password: " + password);
    }

    public void confirmRegistration(String randomUUID) {
        long registrationTime = LocalDateTime.now().atZone(ZoneOffset.UTC).toEpochSecond();
        LOG.info("registrationConfirmTime " + registrationTime);
        UserEntity user = userRepository.findUserEntityByEnabledAndUuidAndRegistrationTimeGreaterThan(false, randomUUID, registrationTime);
        if(user == null){
            throw new UserNotFoundException();
        }
        user.setEnabled(true);
        user.setUuid(null);
        user.setRegistrationTime(registrationTime);
        userRepository.save(user);
    }

    public void recoverPwd(String email) {
        Optional<UserEntity> optUser = userRepository.findUserEntityByEmail(email);
        if(!optUser.isPresent()){
            return;
        }
        long expireRecoveryTime = LocalDateTime.now().plusDays(1).atZone(ZoneOffset.UTC).toEpochSecond();
        String uuid = UUID.randomUUID().toString();
        UserEntity user = optUser.get();
        user.setUuid(uuid);
        user.setRecoveryTime(expireRecoveryTime);
        userRepository.save(user);
        String recoveryLink = "http://localhost:8080/api-pedibus/recover/" + uuid; //FIXME capire come inviare url che non sia hard-coded
        LOG.info("recover: recoveryLink:" + recoveryLink + " expires at " + expireRecoveryTime);
        //mailService.sendMail(email, "Recover your password", "Recover Here " + recoveryLink); //TODO testato che funziona, per adesso tenere commentato

    }

    public String confirmRecovery(String randomUUID, String password) {
        long recoveryTime = LocalDateTime.now().atZone(ZoneOffset.UTC).toEpochSecond();
        LOG.info("recoveryConfirmTime " + recoveryTime);
        UserEntity user = userRepository.
                findUserEntityByEnabledAndUuidAndRecoveryTimeGreaterThan(true, randomUUID, recoveryTime);

        if(user==null){
            throw new UserNotFoundException();
        }
        user.setRecoveryTime(recoveryTime);
        user.setUuid(randomUUID);
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
        return user.getEmail();
    }

    public String findMailByUUID(String randomUUID) {
        UserEntity user =  userRepository.findUserEntityByUuid(randomUUID);
        if(user==null){
            throw new UserNotFoundException();
        }
        return user.getEmail();
    }

    public List<UserResponseDto> getUsers(String line) {
        if(line!=null){
            List<Role> roles = roleRepository.findAllByLine_line(line);
            List<UserEntity> users = roles.stream().map(Role::getUserEntity).collect(Collectors.toList());
            return  getUsersResponseDto(users);
        }
        else{
            Iterable<UserEntity> _users = userRepository.findAll();
            List<UserEntity> users = StreamSupport.stream(_users.spliterator(), false).collect(Collectors.toList());
            return getUsersResponseDto(users);
        }

    }

    private List<UserResponseDto> getUsersResponseDto(List<UserEntity> users) {
        return users.
                stream().
                collect(Collectors.groupingBy(UserEntity::getEmail, Collectors.mapping(Function.identity(), Collectors.toList()))).
                values().stream().
                map(userList -> {
                    UserEntity user = userList.get(0);
                    return UserResponseDto.builder().
                            name(user.getName()).
                            surname(user.getSurname()).
                            email(user.getEmail()).
                            roles(getUserLineGrants(user)).
                            enable(user.isEnabled()).
                            build(); }).
                collect(Collectors.toList());
    }

    @Transactional
    public void addRemoveUserGrant(boolean enable, String line, String email, UserRole userRole) {

        LOG.info("UserService.addRemoveUserGrant - user " + email + " with " + userRole.toString() + "grant on line " + line);

        Optional<Line> _line = lineRepository.findById(line);
        Optional<UserEntity> _user = userRepository.findUserEntityByEmail(email);

        if(!_line.isPresent())
        {
            throw new LineNotFoundException();
        }
        if(!_user.isPresent())
        {
            throw new UserNotFoundException();
        }

        Line l = _line.get();
        UserEntity user = _user.get();

        if(!enable){

            Optional<Role> optionalRole = l.getRoles().stream().filter(role -> role.getLine().getLine().equals(line) && role.getUserRole().equals(userRole) && role.getUserEntity().getEmail().equals(email)).findFirst();
            boolean removed_1 = l.getRoles().removeIf(role -> role.getLine().getLine().equals(line) && role.getUserRole().equals(userRole) && role.getUserEntity().getEmail().equals(email));
            boolean removed_2 = user.getRoles().removeIf(role -> role.getLine().getLine().equals(line) && role.getUserRole().equals(userRole) && role.getUserEntity().getEmail().equals(email));

            if(!optionalRole.isPresent() || !removed_1 && !removed_2){
                throw new RoleNotFoundException();
            }

            roleRepository.delete(optionalRole.get());
        }
        else{

            Role role = new Role(l , _user.get(), userRole);
            List<Role> roles = l.getRoles();
            if (roles == null){
                roles = new ArrayList<>();
            }
            roles.add(role);

            lineRepository.save(l);
        }

        LOG.info(enable? "UserService.addRemoveUserGrant - role added" : "UserService.addRemoveUserGrant - role removed");

    }

    @Transactional
    public void registerChild(String name, String surname, String email) {

        Optional<UserEntity> optionalUser = userRepository.findUserEntityByEmail(email);

        if(!optionalUser.isPresent()){
            throw new UserNotFoundException();
        }

        UserEntity user = optionalUser.get();

        List<Child> children = user.getChildren();

        if(children == null){
            children = new ArrayList<>();
        }

        if(children.stream().anyMatch(child -> child.getName().equals(name) && child.getSurname().equals(surname))){
            throw new ChildConflictException();
        }

        children.add(new Child(user,name,surname, null));

        userRepository.save(user);

    }

    /*
        for each userRole i collect all the lines with that grant fot the user
     */
    private Map<UserRole,List<String>> getUserLineGrants(UserEntity user){
        return user.getRoles().stream().
                collect(Collectors.groupingBy(Role::getUserRole, Collectors.mapping(role -> role.getLine().getLine(), Collectors.toList())));
    }

    /*
    for each userRole i collect the lines
   */
    private Map<UserRole,List<String>> getUserLineGrants(UserEntity user, String line){
        return user.getRoles().stream().filter(role -> role.getLine().getLine().equals(line)).
                collect(Collectors.groupingBy(Role::getUserRole, Collectors.mapping(role -> role.getLine().getLine(), Collectors.toList())));
    }

    @Transactional
    public void updatePassword(String currentPassword, String newPassword) {
        UserEntity user = this.contextService.getContextUser();

        if(!passwordEncoder.matches(currentPassword,user.getPassword())){
            throw new AccessDeniedException("Wrong Password");
        }

        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
    }

    @Transactional
    public void addRegistrationToken(String registrationToken) {

        UserEntity user = this.contextService.getContextUserWithException();
        Set<String> registrationTokens = user.getRegistrationsToken();
        if(registrationToken == null){
            registrationTokens = new HashSet<>();
        }
        registrationTokens.add(registrationToken);
        this.userRepository.save(user);
    }

    /*
    private UserResponseDto getUserLineResponseDto(User user, String line) {
        return UserResponseDto.
                builder().
                name(user.getName()).
                surname(user.getSurname()).
                email(user.getEmail()).
                roles(getUserLineGrants(user,line)).
                enable(user.isEnabled()).
                build();
    }*/

}
