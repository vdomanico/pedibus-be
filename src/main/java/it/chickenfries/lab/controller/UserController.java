package it.chickenfries.lab.controller;

import it.chickenfries.lab.dto.user.RecoverRequestDto;
import it.chickenfries.lab.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class.getCanonicalName());
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/recover/{randomUUID}")
    public String getPageForRecoveryPassword(@PathVariable(value = "randomUUID") String randomUUID, Model model) {

        try{
            String mail = userService.findMailByUUID(randomUUID);
            model.addAttribute("UUID", randomUUID);
            model.addAttribute("MAIL", mail);
            LOGGER.info("Request for recovery " + mail);
        }catch (Exception e){
            LOGGER.info("Invalid mail/uuid");
            return "error";
        }
        return "recovery";
    }

    @PostMapping(value = "/recover/{randomUUID}")
    public String confirmPasswordRecovery(@PathVariable(value = "randomUUID") String randomUUID,
                                          @Valid @ModelAttribute("recoverDto") RecoverRequestDto recoverRequestDto,
                                          Model model) {
        try{
            String mail = userService.confirmRecovery(randomUUID, recoverRequestDto.getPassword());
            model.addAttribute("MAIL", mail);
            LOGGER.info("Recovery success: true");
        } catch(Exception e){
            LOGGER.info("Recovery success: false");
            return "error";
        }
        return "success";
    }

}
