package it.chickenfries.lab.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@Service
public class FirebaseCloudMessagingInitializer {

    @Value("${firebase-conf}")
    private String firebaseConfigPath;
    private Logger logger = LoggerFactory.getLogger(FirebaseCloudMessagingInitializer.class);


    @PostConstruct
    public void initialize() {
        try {
            FirebaseOptions options = new FirebaseOptions.
                    Builder().
                    setCredentials(GoogleCredentials.
                            fromStream(new ByteArrayInputStream(firebaseConfigPath.getBytes()))).
                            build();
            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
                logger.info("Firebase application has been initialized");
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }


}
