package it.chickenfries.lab.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggerConfig {

    private static final Logger LOG = LogManager.getLogger(LoggerConfig.class.getCanonicalName());
    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.registerModule(new JavaTimeModule());
        MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Before("within(it.chickenfries.lab.restcontroller.*)")
    public void endpointBefore(JoinPoint p) {
        LOG.info(p.getTarget().getClass().getSimpleName() + "." + p.getSignature().getName() + " START");
        Object[] signatureArgs = p.getArgs();

        try {
            if (signatureArgs!= null && signatureArgs.length > 0 && signatureArgs[0] != null) {
                LOG.info("Request object: " + MAPPER.writeValueAsString(signatureArgs[0]));
            }
        } catch (Exception e) {
            LOG.error(e);
        }
    }

    @AfterReturning(value = ("within(it.chickenfries.lab.restcontroller.*)"), returning = "returnValue")
    public void endpointAfterReturning(JoinPoint p, Object returnValue) {
        try {
            LOG.info("Response object: " + MAPPER.writeValueAsString(returnValue));
        } catch (JsonProcessingException e) {
            LOG.error(e);
        }
        LOG.info(p.getTarget().getClass().getSimpleName() + "." + p.getSignature().getName() + " END");
    }

    @AfterThrowing(pointcut = ("within(it.chickenfries.lab.restcontroller.*)"), throwing = "e")
    public void endpointAfterThrowing(JoinPoint p, Exception e) throws Exception {
        LOG.error(p.getTarget().getClass().getSimpleName() + " " + p.getSignature().getName() + " " + e.getMessage());
    }

}
