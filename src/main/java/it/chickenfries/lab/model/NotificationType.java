package it.chickenfries.lab.model;

public enum NotificationType {

    SUCCESS,
    ERROR,
    WARNING,
    INFO,
    REFRESH,
    MONITOR,
    UNREAD_MESSAGES

}
