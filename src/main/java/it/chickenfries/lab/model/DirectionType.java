package it.chickenfries.lab.model;

public enum DirectionType {
    TO_SCHOOL, TO_HOME
}
