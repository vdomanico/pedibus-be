package it.chickenfries.lab.model;

public enum UserRole {

    LINE_ADMINISTRATOR,
    CHAPERON,
    PARENT

}
