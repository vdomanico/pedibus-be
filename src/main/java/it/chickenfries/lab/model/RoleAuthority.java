package it.chickenfries.lab.model;

import org.springframework.security.core.GrantedAuthority;

public class RoleAuthority implements GrantedAuthority {

    private UserRole userRole;
    private String line;

    public RoleAuthority(UserRole userRole, String line) {
        this.userRole = userRole;
        this.line = line;
    }

    @Override
    public String getAuthority() {
        return userRole.toString() + ": " + line;
    }
}
