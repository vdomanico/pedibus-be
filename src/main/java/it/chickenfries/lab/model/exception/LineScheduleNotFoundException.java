package it.chickenfries.lab.model.exception;

public class LineScheduleNotFoundException extends GenericNotFoundException {
    public LineScheduleNotFoundException() {
        super("Schedule Not Found");
    }

    public LineScheduleNotFoundException(String message) {
        super(message);
    }
}
