package it.chickenfries.lab.model.exception;

public class ShiftNotModifiableException extends GenericNotFoundException {
    public ShiftNotModifiableException() {
        super("Shift Not Modifiable - Pedibus already passed");
    }
}
