package it.chickenfries.lab.model.exception;

public class RoleNotFoundException extends GenericNotFoundException {

    public RoleNotFoundException() {
        super("Role not found");
    }

    public RoleNotFoundException(String message) {
        super(message);
    }
}
