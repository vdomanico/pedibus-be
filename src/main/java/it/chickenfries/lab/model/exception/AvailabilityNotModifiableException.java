package it.chickenfries.lab.model.exception;

public class AvailabilityNotModifiableException extends GenericNotFoundException {
    public AvailabilityNotModifiableException() {
        super("Availability Not Modifiable - Shift already closed");
    }
}
