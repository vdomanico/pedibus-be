package it.chickenfries.lab.model.exception;

public class ReservationNotAddedException extends GenericConflictException {
    public ReservationNotAddedException() { super("Reservation Not Found"); }
}
