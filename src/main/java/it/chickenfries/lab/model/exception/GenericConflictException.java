package it.chickenfries.lab.model.exception;

public class GenericConflictException extends RuntimeException {

    public GenericConflictException() {
        super("Generic conflict");
    }

    public GenericConflictException(String message) {
        super(message);
    }
}
