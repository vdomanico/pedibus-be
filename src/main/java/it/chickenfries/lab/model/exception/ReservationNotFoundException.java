package it.chickenfries.lab.model.exception;

public class ReservationNotFoundException extends GenericNotFoundException {

    public ReservationNotFoundException() {
        super("Reservation Not Found");
    }

    public ReservationNotFoundException(String message) {
        super(message);
    }
}
