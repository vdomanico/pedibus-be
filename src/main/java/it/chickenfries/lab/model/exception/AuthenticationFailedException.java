package it.chickenfries.lab.model.exception;

public class AuthenticationFailedException extends GenericUnauthorizedException {

    public AuthenticationFailedException() {
        super("Authentication Failed Exception");
    }

    public AuthenticationFailedException(String message) {
        super(message);
    }
}
