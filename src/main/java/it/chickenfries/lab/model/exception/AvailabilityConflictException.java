package it.chickenfries.lab.model.exception;

public class AvailabilityConflictException extends GenericConflictException {
    public AvailabilityConflictException() {
        super("User already gave his availability");
    }

    public AvailabilityConflictException(String message) {
        super(message);
    }
}
