package it.chickenfries.lab.model.exception;

public class LineStopNotFoundException extends GenericNotFoundException {
    public LineStopNotFoundException() {
        super("Line Not Found Exception");
    }

    public LineStopNotFoundException(String message) {
        super(message);
    }
}
