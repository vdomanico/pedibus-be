package it.chickenfries.lab.model.exception;

public class ChildNotFoundException extends GenericNotFoundException {

    public ChildNotFoundException() {
        super("Child Not Found");
    }

    public ChildNotFoundException(String message) {
        super(message);
    }

}
