package it.chickenfries.lab.model.exception;

public class MessageNotFoundException extends GenericNotFoundException {

    public MessageNotFoundException() {
        super("Message Not Found");
    }

    public MessageNotFoundException(String message) {
        super(message);
    }
}
