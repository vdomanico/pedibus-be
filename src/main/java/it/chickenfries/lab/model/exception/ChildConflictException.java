package it.chickenfries.lab.model.exception;

public class ChildConflictException extends GenericConflictException {
    public ChildConflictException() {
        super("Child already registered with this user");
    }

    public ChildConflictException(String message) {
        super(message);
    }
}
