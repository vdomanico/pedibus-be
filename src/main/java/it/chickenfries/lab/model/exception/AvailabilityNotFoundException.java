package it.chickenfries.lab.model.exception;

public class AvailabilityNotFoundException extends GenericNotFoundException {

    public AvailabilityNotFoundException() {
        super("Availability Not Found");
    }

}
