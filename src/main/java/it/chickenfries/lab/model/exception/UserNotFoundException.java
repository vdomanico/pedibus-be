package it.chickenfries.lab.model.exception;

public class UserNotFoundException extends GenericNotFoundException {

    public UserNotFoundException() {
        super("User Not Found");
    }

}
