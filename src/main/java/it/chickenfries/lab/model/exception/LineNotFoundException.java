package it.chickenfries.lab.model.exception;

public class LineNotFoundException extends GenericNotFoundException {

    public LineNotFoundException() {
        super("Line Not Found");
    }

    public LineNotFoundException(String message) {
        super(message);
    }
}
