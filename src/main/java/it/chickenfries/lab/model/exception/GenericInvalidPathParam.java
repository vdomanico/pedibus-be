package it.chickenfries.lab.model.exception;

public class GenericInvalidPathParam extends RuntimeException {

    public GenericInvalidPathParam() {
        super("Inavlid Path Paramter");
    }

    public GenericInvalidPathParam(String message) {
        super(message);
    }
}
