package it.chickenfries.lab.model.exception;

public class ReservationConflictException extends GenericConflictException {
    public ReservationConflictException() {
        super("Child already reserved for this round");
    }

    public ReservationConflictException(String message) {
        super(message);
    }
}
