package it.chickenfries.lab.model.exception;

public class GenericUnauthorizedException extends RuntimeException{

    public GenericUnauthorizedException() {
        super("Unauthorized");
    }

    public GenericUnauthorizedException(String message) {
        super(message);
    }
}
