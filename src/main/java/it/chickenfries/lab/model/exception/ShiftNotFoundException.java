package it.chickenfries.lab.model.exception;

public class ShiftNotFoundException extends GenericNotFoundException {

    public ShiftNotFoundException() {
        super("Shift Not Found");
    }

}
