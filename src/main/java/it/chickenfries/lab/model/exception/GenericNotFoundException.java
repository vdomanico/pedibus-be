package it.chickenfries.lab.model.exception;

public class GenericNotFoundException extends RuntimeException{

    public GenericNotFoundException() {
        super("Not Found");
    }

    public GenericNotFoundException(String message) {
        super(message);
    }
}
