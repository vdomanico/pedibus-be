package it.chickenfries.lab.model.exception;

public class InvalidJwtAuthenticationException extends GenericUnauthorizedException {

    public InvalidJwtAuthenticationException(String e){
        super(e);
    }
}
