package it.chickenfries.lab.model.exception;

public class UserConflictException extends GenericConflictException {
    public UserConflictException() {
        super("User already registered with this email");
    }

    public UserConflictException(String message) {
        super(message);
    }
}
