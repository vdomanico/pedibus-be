package it.chickenfries.lab.model.exception;

public class ShiftConflictException extends GenericConflictException {
    public ShiftConflictException() {
        super("User already reserved for this shift");
    }

    public ShiftConflictException(String message) {
        super(message);
    }
}
