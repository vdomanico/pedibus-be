package it.chickenfries.lab.model.exception;

public class ReservationNotUpdatableException extends GenericNotFoundException {

    public ReservationNotUpdatableException() {
        super("Reservation not updatable - Pedibus already passed");
    }

    public ReservationNotUpdatableException(String message) {
        super(message);
    }

}
