package it.chickenfries.lab.model.exception;

public class ReservationNotEraseableException extends GenericConflictException {
    public ReservationNotEraseableException() {
        super("Reservation not eraseable - Pedibus already passed");
    }

    public ReservationNotEraseableException(String message) {
        super(message);
    }
}