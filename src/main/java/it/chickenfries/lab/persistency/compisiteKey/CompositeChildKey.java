package it.chickenfries.lab.persistency.compisiteKey;

import it.chickenfries.lab.persistency.entity.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompositeChildKey implements Serializable {

    private UserEntity userEntity;
    private String name;
    private String surname;

}
