package it.chickenfries.lab.persistency.compisiteKey;

import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.persistency.entity.Child;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompositeReservationKey implements Serializable {

    private LocalDate date;
    private Child child;
    private DirectionType direction;
}
