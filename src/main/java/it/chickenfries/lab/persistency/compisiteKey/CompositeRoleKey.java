package it.chickenfries.lab.persistency.compisiteKey;

import it.chickenfries.lab.persistency.entity.Line;
import it.chickenfries.lab.persistency.entity.UserEntity;
import it.chickenfries.lab.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompositeRoleKey implements Serializable {

    private Line line;
    private UserEntity userEntity;
    private UserRole userRole;


}
