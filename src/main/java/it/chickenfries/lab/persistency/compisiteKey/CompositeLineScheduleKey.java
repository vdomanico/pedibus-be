package it.chickenfries.lab.persistency.compisiteKey;

import it.chickenfries.lab.persistency.entity.Line;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompositeLineScheduleKey implements Serializable {

    private Line line;
    private LocalDate day;

}
