package it.chickenfries.lab.persistency.compisiteKey;

import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.persistency.entity.Child;
import it.chickenfries.lab.persistency.entity.LineSchedule;
import it.chickenfries.lab.persistency.entity.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompositePresenceKey implements Serializable {

    private UserEntity user;
    private LineSchedule lineSchedule;
    private DirectionType directionType;
    private Child child;

}
