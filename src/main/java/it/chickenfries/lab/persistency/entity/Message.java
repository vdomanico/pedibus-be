package it.chickenfries.lab.persistency.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Message {

    @Id private String id;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="userEntity", referencedColumnName = "email", foreignKey = @ForeignKey(name = "FK_MESSAGE_TO_USER")) private UserEntity userEntity;
    private String subject;
    private String message;
    private LocalDateTime sentDateTime;
    private LocalDateTime readDateTime;
    boolean read;

}
