package it.chickenfries.lab.persistency.entity;

import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.persistency.compisiteKey.CompositeReservationKey;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@IdClass(CompositeReservationKey.class)
@Entity
@ToString(exclude = {"lineSchedule","on_B","off_B", "userEntity", "child"})
@EqualsAndHashCode(exclude = {"lineSchedule","on_B","off_B", "userEntity", "child"})
public class Reservation implements Serializable {

    @ManyToOne @JoinColumns({
            @JoinColumn(name="line", referencedColumnName = "line", foreignKey = @ForeignKey(name = "FK_RESERVATION_TO_LINE_SCHEDULE_LINE")),
            @JoinColumn(name="day", referencedColumnName = "day", foreignKey = @ForeignKey(name = "FK_RESERVATION_TO_LINE_SCHEDULE_DATE"))})
    private LineSchedule lineSchedule;

    @ManyToOne @JoinColumn(name="line_stop_on_B", referencedColumnName = "place", foreignKey = @ForeignKey(name = "FK_RESERVATION_TO_LINE_STOP_ON_B")) private LineStop on_B;
    @ManyToOne @JoinColumn(name="line_stop_off_B", referencedColumnName = "place", foreignKey = @ForeignKey(name = "FK_RESERVATION_TO_LINE_STOP_OFF_B")) private LineStop off_B;

    /*
   The "applier" registers the kid so we keep tracks which user registered the kid (may not be the parent but the line administrator i.e.)
    */
    @ManyToOne @JoinColumn(name="applier", referencedColumnName = "email", foreignKey = @ForeignKey(name = "FK_RESERVATION_TO_USER")) private UserEntity userEntity;
    /*
   The child belongs to the real user here, which means the parent
    */
    @Id @ManyToOne @JoinColumns({
            @JoinColumn(name="child", referencedColumnName = "email", foreignKey = @ForeignKey(name = "FK_RESERVATION_TO_CHILD_USER")),
            @JoinColumn(name="name", referencedColumnName = "name", foreignKey = @ForeignKey(name = "FK_RESERVATION_TO_CHILD_NAME")),
            @JoinColumn(name="surname", referencedColumnName = "surname", foreignKey = @ForeignKey(name = "FK_RESERVATION_TO_CHILD_SURNAME"))})
    private Child child;

    @Column(unique = true) private String ticket;
    @Id private DirectionType direction;
    @Id private LocalDate date;

    private boolean collected;
    private boolean delivered;
    private boolean deleted;

}
