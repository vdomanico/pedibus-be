package it.chickenfries.lab.persistency.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;


@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"roles", "reservations", "shifts", "availabilities", "children", "messages"})
@EqualsAndHashCode(exclude = {"roles", "reservations", "shifts", "availabilities", "children", "messages"})
public class UserEntity implements Serializable {

    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.ALL)
    private List<Reservation> reservations;

    @OneToMany(mappedBy = "userEntity", cascade=CascadeType.ALL)
    private List<Shift> shifts;

    @OneToMany(mappedBy = "userEntity", cascade=CascadeType.ALL)
    private List<Availability> availabilities;

    @OneToMany(mappedBy = "userEntity", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Role> roles;

    @OneToMany(mappedBy = "userEntity", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Child> children;

    @OneToMany(mappedBy = "userEntity", cascade=CascadeType.ALL)
    private List<Message> messages;

    @Id @Column(unique = true) private String email;

    @ElementCollection private Set<String> registrationsToken;

    private String name;
    private String surname;
    private String password;
    private String uuid;
    private boolean enabled;
    private long registrationTime;
    private long recoveryTime;


}


