package it.chickenfries.lab.persistency.entity;

import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.persistency.compisiteKey.CompositeShiftKey;
import lombok.*;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@IdClass(CompositeShiftKey.class)
@Entity
@ToString(exclude = {"lineSchedule", "userEntity"})
@EqualsAndHashCode(exclude = {"lineSchedule", "userEntity"})
public class Shift {

    @Id
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name="line", referencedColumnName = "line", foreignKey = @ForeignKey(name = "FK_SHIFT_TO_LINE_SCHEDULE_LINE")),
            @JoinColumn(name="day", referencedColumnName = "day", foreignKey = @ForeignKey(name = "FK_SHIFT_TO_LINE_SCHEDULE_DATE"))})
    private LineSchedule lineSchedule;

    @Id @ManyToOne @JoinColumn(name="chaperon", referencedColumnName = "email", foreignKey = @ForeignKey(name = "FK_SHIFT_TO_USER")) private UserEntity userEntity;

    @Id private DirectionType direction;

}
