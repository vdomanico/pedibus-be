package it.chickenfries.lab.persistency.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"stops", "lineSchedules", "roles"})
@EqualsAndHashCode(exclude = {"stops", "lineSchedules", "roles"})
public class Line implements Serializable {
    @Id private String line;
    @OneToMany(mappedBy = "line", cascade=CascadeType.ALL) private List<LineStop> stops;
    @OneToMany(mappedBy = "line", cascade=CascadeType.ALL) private List<LineSchedule> lineSchedules;
    @OneToMany(mappedBy = "line", cascade=CascadeType.ALL) private List<Role> roles;
}
