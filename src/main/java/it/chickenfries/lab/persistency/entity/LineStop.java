package it.chickenfries.lab.persistency.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Entity(name = "LINESTOP")
@ToString(exclude = "line")
@EqualsAndHashCode(exclude = "line")
public class LineStop implements Serializable {

    @Id private String place;
    @ManyToOne @JoinColumn(name = "line", foreignKey = @ForeignKey(name = "FK_LINE_STOP_TO_LINE")) private Line line;
    @OneToMany(mappedBy = "on_B") private List<Reservation> on_board;
    @OneToMany(mappedBy = "off_B") private List<Reservation> off_board;

    private Time toSchool;
    private Time toHome;
    private double lat;
    private double lon;

}
