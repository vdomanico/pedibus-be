package it.chickenfries.lab.persistency.entity;

import it.chickenfries.lab.persistency.compisiteKey.CompositeLineScheduleKey;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@IdClass(CompositeLineScheduleKey.class)
@ToString(exclude = {"line"})
@EqualsAndHashCode(exclude = {"line"})
public class LineSchedule implements Serializable {

    @Id @JoinColumn(name="line", referencedColumnName = "line", foreignKey = @ForeignKey(name = "FK_LINE_SCHEDULE_TO_LINE")) @ManyToOne private Line line;
    @Id private LocalDate day;

    @OneToMany(mappedBy = "lineSchedule", cascade=CascadeType.ALL) private List<Reservation> reservations;
    @OneToMany(mappedBy = "lineSchedule", cascade=CascadeType.ALL) private List<Shift> shifts;
    @OneToMany(mappedBy = "lineSchedule", cascade=CascadeType.ALL) private List<Availability> availabilities;
}
