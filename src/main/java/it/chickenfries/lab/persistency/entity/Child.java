package it.chickenfries.lab.persistency.entity;

import it.chickenfries.lab.persistency.compisiteKey.CompositeChildKey;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(CompositeChildKey.class)
@ToString(exclude = {"reservations"})
@EqualsAndHashCode(exclude = {"reservations"})
public class Child implements Serializable {

    @Id @ManyToOne @JoinColumn(name = "email", referencedColumnName = "email", foreignKey = @ForeignKey(name = "FK_CHILD_TO_USER")) private UserEntity userEntity;
    @Id private String name;
    @Id private String surname;

    @OneToMany(mappedBy = "child", cascade=CascadeType.ALL)
    private List<Reservation> reservations;

}
