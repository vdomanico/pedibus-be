package it.chickenfries.lab.persistency.entity;

import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.persistency.compisiteKey.CompositeRoleKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(CompositeRoleKey.class)

public class Role implements Serializable {

    @Id @ManyToOne @JoinColumn(name="line", referencedColumnName = "line", foreignKey = @ForeignKey(name = "FK_ROLE_TO_LINE")) private Line line;
    @Id @ManyToOne @JoinColumn(name = "email", referencedColumnName = "email", foreignKey = @ForeignKey(name = "FK_ROLE_TO_USER")) private UserEntity userEntity;
    @Id private UserRole userRole;
}
