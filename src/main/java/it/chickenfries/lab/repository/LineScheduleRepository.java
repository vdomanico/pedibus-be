package it.chickenfries.lab.repository;

import it.chickenfries.lab.persistency.entity.LineSchedule;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface LineScheduleRepository extends CrudRepository<LineSchedule,Long> {

    Optional<LineSchedule> findByDayAndLine_Line(LocalDate day, String line);
    List<LineSchedule> findAllByLine_line(String line);

}
