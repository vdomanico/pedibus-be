package it.chickenfries.lab.repository;

import it.chickenfries.lab.persistency.entity.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    Page<Message> findAllByUserEntity_Email(String email, Pageable pageable);
    Optional<Message> findById(String id);
    int countAllByUserEntity_EmailAndRead(String email, boolean read);

}
