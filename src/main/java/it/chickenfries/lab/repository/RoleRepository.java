package it.chickenfries.lab.repository;

import it.chickenfries.lab.model.UserRole;
import it.chickenfries.lab.persistency.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, String> {
    Optional<Role> findByUserEntity_EmailAndUserEntity_EnabledAndUserRoleAndLine_Line(String email, boolean enabled, UserRole userRole, String line);
    List<Role> findByUserEntity_EmailAndUserEntity_EnabledAndUserRole(String email, boolean enabled, UserRole userRole);
    List<Role> findAllByLine_line(String line);
}
