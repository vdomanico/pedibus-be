package it.chickenfries.lab.repository;

import it.chickenfries.lab.persistency.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, String> {

    Optional<UserEntity> findUserEntityByEmail(String email);

    UserEntity findUserEntityByUuid(String uuid);

    Optional<UserEntity> findUserEntityByEmailAndEnabled(String email, boolean enabled);

    UserEntity findUserEntityByEnabledAndUuidAndRegistrationTimeGreaterThan(boolean enabled, String UUID, long now);

    UserEntity findUserEntityByEnabledAndUuidAndRecoveryTimeGreaterThan(boolean enabled, String UUID, long now);

}
