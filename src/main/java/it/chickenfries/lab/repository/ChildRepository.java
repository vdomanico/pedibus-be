package it.chickenfries.lab.repository;

import it.chickenfries.lab.persistency.entity.Child;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChildRepository extends CrudRepository<Child, String> {
    Optional<Child> findByUserEntity_EmailAndName_AndSurname(String email, String name, String surname);
}
