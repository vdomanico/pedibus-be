package it.chickenfries.lab.repository;

import it.chickenfries.lab.persistency.entity.Line;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LineRepository extends CrudRepository<Line,String> {

    Optional<Line> findOneByLine(String line);
    Optional<Line> findByLine(String line);
}
