package it.chickenfries.lab.repository;

import it.chickenfries.lab.persistency.entity.LineStop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LineStopRepository extends CrudRepository<LineStop,Long> {}
