package it.chickenfries.lab.repository;

import it.chickenfries.lab.model.DirectionType;
import it.chickenfries.lab.persistency.entity.Child;
import it.chickenfries.lab.persistency.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, String> {

    Optional<Reservation> findByTicket(String ticket);
    List<Reservation> findByDateAndDirection(LocalDate date, DirectionType direction);
    List<Reservation> findByDateAndChild(LocalDate date, Child child);

}
