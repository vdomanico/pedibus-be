package it.chickenfries.lab.util;

import it.chickenfries.lab.model.exception.GenericInvalidPathParam;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class DateUtil {

    public static LocalDate getDateFromPathParam(String date){
        LocalDate res;
        try { res = LocalDate.parse(date); }
        catch(DateTimeParseException e){
            throw new GenericInvalidPathParam("Invalid Date Format: yyMMdd");  }
        return res;
    }
}
